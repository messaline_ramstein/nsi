# Cours de SNT Lycée Marguerite de Flandre

[diaporama présentation clase de 2nde](./presentation_2nde.odp)

## Introduction: Le Numérique, c'est quoi?

[introduction](seanceintro.docx)

## T1.Le web

[introduction](./T1.Le%20web/Intro%20Web.pptx)

[cours](./T1.Le%20web/cours_web_versioneleve.docx)

[vidéo site internet ou site web?](./T1.Le%20web/MOOC%20SNT%20_%20Le%20web,%20site%20internet ou%20site%20web _%20-%20YouTube%20(360p).mp4)

[vidéo cookies](./T1.Le%20web/cookies.mp4)

[exercices](./T1.Le%20web/exosweb.odt)

### TP1

[logo.png](./T1.Le%20web/TP1/logo.png) &nbsp; [TPWeb1.html](./T1.Le%20web/TP1/TPweb1.html) &nbsp; [énoncé TP1](./T1.Le%20web/TP1/TPweb1.docx)

### TP2

[calissons_aix.jpg](./T1.Le%20web/TP2/calissons_aix.jpg) &nbsp; [textebrut_calissons.html](./T1.Le%20web/TP2/textebrut_calissons.html) &nbsp; [énoncé TP2](./T1.Le%20web/TP2/TPweb2.docx)

### TP3

[logo.png](./T1.Le%20web/TP3/logo.png) &nbsp; [TPWeb3.html](./T1.Le%20web/TP3/TPweb3.html) &nbsp; [style.css](./T1.Le%20web/TP3/style.css) &nbsp; [énoncé TP3](./T1.Le%20web/TP3/TPweb3.docx)


## T2. Les réseaux sociaux

[cours réseaux sociaux](T2.Les%20réseaux%20sociaux/cours%20les%20reseaux%20sociaux_corr%20.docx)

[Activité Panorama des réseaux sociaux](./T2.Les%20réseaux%20sociaux/Activite-1-Panorama-des-Reseaux-sociaux.docx)

## T3. La photographie Numérique

[cours](./T3.La%20photographie%20numérique/courphotoprof.docx) &nbsp; [multicolors.jpg](./T3.La%20photographie%20numérique/multicolors.jpg)

[vidéo fonctionnement d'un appareil photo numérique](./T3.La%20photographie%20numérique/Fonctionnement%20d'un%20appareil%20photo%20numérique%20-%20PARTIE%201.mp4)

[TP1. Traitement d'une image](./T3.La%20photographie%20numérique/TP%20Traitement%20image%20correction.docx) &nbsp; [pomme.jpg](./T3.La%20photographie%20numérique/pomme.jpg)

[TP2. Dématriçage](./T3.La%20photographie%20numérique/2%20dematricage%20prof.xls)

## T4. internet

[cours](./T4.Internet/cours_Internet_eleve.doc) &nbsp; [vidéo réseau physique](./T4.Internet/les-reseaux-informatiques-et-internet%20-%20Copie.mp4)

[TP1](./T4.Internet/TPFilius1_2023.odt)

## Programmation Python

[Débuter en Python](./programmation%20python/decouvertePython.odt)

[structures conditionnelles](./programmation%20python/if.odt)

[boucle For](./programmation%20python/bouclefor.odt)

