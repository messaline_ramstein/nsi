# CHAPVIII.                                                                                                         
__Intro : LES SEQUENCES__

• Les __séquences__ sont des manières d’organiser les données, dans des ensembles __ordonnés__, que l’on peut faire évoluer ou non. Ordonné ne signifie pas que les données sont triées, ce qui est une notion relative. Ordonné signifie que la place d’une donnée dans la séquence est fixée par rapport aux autres. 

• Les données contenues dans une séquences peuvent être du même type ou non. Quand elles le sont,on parle de séquence __homogène__. Dans le cas contraire on parle de séquence __hétérogène__. On peut quantifier degré d’homogénéité ou d’hétérogénéité d’une séquence.

• Lorsque les données d’une séquence sont non modifiables, on dit que la séquence est __immuable, non-muable, ou immutable, non-mutable__.  Lorsque les données sont modifiables, on dit que la séquence est __muable ou mutable__.
	

Les trois séquences de base sont :
- les __chaînes de caractères__ qui sont des séquences non-mutables(voir chap I)
- les __p-uplets__, appelés tuples en python, qui sont des séquences non-mutables
- les __tableaux__, appelés __listes__ en python, qui sont des séquences mutables
Il existe d’autre types de séquences comme les séquences ``range()``, les ensembles ``set()``, et les séquences binaires ``b'1000111000'``.