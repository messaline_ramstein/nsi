# CHAP VII. Spécification et tests des programmes

## Exercice 1 : Documentation au format resT 🏆 
1. Soit la fonction suivante renvoyant la somme de deux dés à 6 faces :
```python
def total_des(de1, de2):
    return de1 + de2
```
Ecrire une docstring complète. On rajoutera une contrainte d'utilisation.

2. Soit la procédure suivante affichant le nom, le prénom et la taille en mètres d'une personne :
```python
def affichage_infos(nom, prenom, taille):
    print("Mes informations personnelles :", nom, prenom, str(taille)+"m")
```
Ecrire une docstring complète

## Exercice 2 : Préconditions et postconditions 🏆 
1. Soit la fonction racine carrée appliquée sur un entier : trouver une précondition et deux postconditions (plus difficile) sur son utilisation. On ne s'intéressera pas au type de données.

2. Soit la fonction suivante renvoyant le solde d'un compte bancaire qu'on souhaite toujours crediteur (ou à zéro)
```python
def nouveau_solde(retrait, solde):
    solde = solde - retrait
    return solde
```
a- Trouver une précondition et une postcondition

b- Ecrire la docstring correspondante en ajoutant des contraintes d'utilisations

## Exercice 3 : Assertions 🏆 
1. Soit la fonction suivante mention renvoyant la mention obtenue au BAC pour une moyenne passée en paramètre:
```python
def mention(moyenne):
    if moyenne < 12:
        return None
    elif moyenne < 14:
        return 'AB'
    elif moyenne <16:
        return 'B'
    else :
        return 'TB'
```
a- Ecrire une assertion vérifiant le type du paramètre moyenne puis rédiger la docstring correspondante

 b- Ecrire deux assertions permettant d'éviter des moyennes incohérentes.

2. Soit la fonction suivante renvoyant la valeur maximale présente dans un tableau d'entiers passé en paramètre
```python
def max_tableau(tab):
    max_value = tab[0]
    for nombre in tab :
        if nombre > max_value:
            max_value = nombre
    return max_value
```
a- Quelle est la précondition sur le tableau passé en paramètre? (Penser au cas limite)

b- Ecrire l'assertion correspondante

c- Donner une deuxième version ``max_tableau_bis`` de cette fonction sans utiliser d'assertion mais en utilisant la valeur ``None`` comme valeur de retour


## Exercice 4 : Doctests 🏆 
__1. Fonction puissance__

a- Donner une chaîne de documentation (pour l'instant sans exemple) pour la fonction suivante qui calcule x à la puissance n pour deux entiers x et n.
```python
def puissance(x, n):
    return x ** n
```
b- Etablir un jeu de quelques tests et charger le module ``doctest`` au lancement du script comme vu en cours à partir du shell 

c- Modifier un des tests afin qu'il échoue. Observer le résultat.

__2. Fonction max_tableau__

a- Reprendre la fonction ``max_tableau`` de l'exercice précédent et établir une docstring compléte avec un jeu de tests.

b- Les doctests peuvent aussi s'appliquer aux exceptions levées lors d'une erreur comme ``AssertionError``.
Voici un exemple d'utilisation dans la docstring:
```python
"""
>>> max_tableau([])
Traceback (most recent call last):
    ...
AssertionError: le tableau ne peut être vide

"""
```

__Essayer d'intégrer cet exemple à vos doctests en adaptant le message d'erreur au votre__

## Exercice 5: Bon jeu de tests.
On prétend que le prédicat appartient défini juste après teste l'appartenance de la valeur v au tableau t.
```python
def appartient(v,t):
    i = 0
    while i < len(t)- 1 and t[i] !=v:
        i = i + 1
    return i < len(t) 

Par exemple :
>>> appartient(4, [1,3,4,8])
True
```
1. Etablir un jeu de quelques tests (dans certains cas v est présent dans le tableau, dans d'autres non). Puis les utiliser avec le module ``doctest``. Constater des erreurs car la fonction ne réalise en fait pas ce qu'on attend d'elle.

2. Expliquer ce que fait en réalité la fonction ``appartient``



