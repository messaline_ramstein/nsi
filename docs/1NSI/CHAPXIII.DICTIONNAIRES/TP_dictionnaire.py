######################
##                  ##
##   DICTIONNAIRES  ##
##                  ##
######################

##############
###  COURS ###
##############

# dictionnaire vide
dico_en_fr={}

# ajout des cles et des valeurs
dico_en_fr["yes"]="oui"
dico_en_fr["no"]="non"
dico_en_fr["why"]="pourquoi"

# définition directe d'un dictionnaire
dico_es_fr={"si":"oui","hoy":"aujourd'hui","porque":"pourquoi"}

# définition d'un dictionnaire en utilisant la fonction dict
dico_en_fr_nb=dict(one=1,two=2,three=3)

# transformation d'une liste de tuple à 2 éléments
liste_es_nb=[("uno",1),("dos",2),("tres",3)]
dico_es_nb=dict(liste_es_nb)

# définition d'un dictionnaire par compréhension
suite_carre={x:x**2 for x in range(5)}

# Méthodes keys et values
dico_en_fr.keys()


#################
### EXERCICES ###
#################

## EXERCICE 1  ##

def points(mot):
    scrabble={"A":1,"B":3,"C":3,"D":2,"E":1,"F":4,"G":2,"H":4,"I":1,"J":8,"K":10,"L":1,"M":2,"N":1,"O":1,"P":3,"Q":8,"R":1,"S":1,"T":1,"U":1,"V":4,"W":10,"X":10,"Y":10,"Z":10}
    pass
## EXERCICE 2  ##

def occurrences(liste):
    dict={}
    pass

## EXERCICE 3  ##

def correction_QCM_Alice(reponses_Alice,reponses_valides):
    pass
    
    
