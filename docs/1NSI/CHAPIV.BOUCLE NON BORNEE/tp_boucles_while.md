---
title: "Chapitre IV Boucles non bornées"
subtitle : "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice n°1 : Tours de boucles &#x1F3C6;

Pour chacune des situations suivantes, dresser un tableau de suivi des valeurs des variables utilisées : au moment de l'initialisation puis à chaque tour de boucle.  
En déduire ce qu'affichera le programme puis le vérifier avec Thonny.

__1)__

```python
i = 0
j = 1
while i < 3:
    i = i + 1
    print(i + j)
```

__2)__

```python
i = 1
j = 1
while i < 3:
    i = i + 1
while j < 1:
    j = j + 1
print(i + j)
```

__3)__
```python
x = 5
while x != 2:
    x = x - 1
print(x)
```

__4)__
```python
x = 12
while x > 12:
    x = x + 1
print(x)
```

# Exercice n°2 : Procédures et boucles  &#x1F3C6;

> 💾 En programmation on appelle __procédure__ une fonction sans valeur de retour.

_Par exemple :_

```python
def affichage():
    x = 3
    print(x)
```

`affichage` est une procédure (aucun `return` n'est présent dans le bloc de code), cette distinction n'est pas utilisée dans le langage Python mais peut l'être dans d'autres : nous emploierons donc ce mot par la suite.

__1)__ Ecrire une procédure `afficher_carre` sans parametre qui affiche les carrés des chiffres de 0 à 3.

```python
>>> afficher_carre()
 0
 1
 4
 9
```

__2)__ Ecrire une procédure `afficher_ligne` acceptant un paramètre `n` : le nombre de caractères `|` qui devront s'afficher les uns en dessous des autres

```python
>>> afficher_ligne(3)
 |
 |
 |
```

💾 _Notes_ :  

> * Le caractère __`|`__ est appelé barre verticale (ou __pipe__). Il est obtenu à l'aide de la combinaison de touches `alt gr` + `6`      
> * A l'affichage de caractères grâce à la fonction `print`, remarquez que les guillemets n'apparaissent pas. C'est la différence entre l'évaluation d'une chaîne de caractères dans le Shell comme rencontré précédemment et son __affichage__.  

__3)__ Ecrire une procédure `afficher_triangle` acceptant un paramètre `n` qui affiche un triangle de hauteur __n__ sur le modèle de l'exemple ci-dessous.

```python
>>> afficher_triangle(4)
 o
 oo
 ooo
 oooo
```

__4)__ 🥇 Ecrire une procédure `afficher_diagonale` acceptant un paramètre `n` qui affiche une diagonale de __n__ caractères `.` sur le modèle de l'exemple ci-dessous.

```python
>>> afficher_diagonale(5)
 .
   .
     .
       .
         .
```

# Exercice n°3 : Tables de multiplication  &#x1F3C6;  &#x1F3C6; 

__1)__ Ecrire une procédure `afficher_multiples_2` affichant les dix premiers multiples de 2

__2)__ Ecrire une procédure `afficher_table_2` affichant la table de multiplication de 2 ainsi :  

```python
>>> afficher_table_2()
 2 x 1 = 2
 2 x 2 = 4
 2 x 3 = 6
 2 x 4 = 8
 2 x 5 = 10
 2 x 6 = 12
 2 x 7 = 14
 2 x 8 = 16
 2 x 9 = 18
 2 x 10 = 20
```

💾 _AIDE_ :  

> On peut utiliser la fonction `print` ainsi : `print(arg1,arg2,arg2,...)` :   
```python  
  >>> print("Il est", 10 ,"heures")
   Il est 10 heures
```  

> Chaque argument séparé par une `,` est converti en chaîne de caractères peu importe son type !  
> Remarquez que des espaces sont automatiquement insérés par défaut entre les élements à afficher.  

__3)__ Ecrire une procédure `afficher_table` qui affiche la table de multiplication d'un entier `n` passé en paramètre.  

# Exercice n°4 : La boucle "tant que" est non bornée  &#x1F3C6; 

Tester le code suivant

```python
x = 0
while x < 1 :
    print("Je ne m'arrête jamais")
```
__1)__ Expliquer  le problème rencontré   

__2)__ Pour chacun des deux programmes indiquer ce qu'il fait

1er programme : 

```python
a = int(input("a : " ))
b = int(input("b : " ))
i = 0
m = 0
while i < b :
    m = m + a
    i = i + 1
print("a * b = ", m)
```

2eme programme : 

```python
a = int(input())
i = 0
while a > 0 :
    a = a // 10
    i = i + 1
print(i)
```

3eme programme : 

```python
a = int(input())
k = 1
i = 0
while k < a + 1 :
    k = k * 10
    i = i + 1
print(i)
```

# Exercice n°5 : Implémentation d'un algorihtme &#x1F3C6; &#x1F3C6;

On souhaite implémenter en Python l'__algorithme donnant le nombre de bits nécessaires à la représentation en base 2 d'un entier naturel non nul__.

```
Entrée : n : nombre entier
Sortie : nombre de bits nécessaires à la représentation en base 2

1:  bits ← 1
2:  tant que n > 1 faire
3:    n ← n ÷ 2             // la division est entière
4:    bits ← bits + 1
4:  fin tant que
5:  renvoyer bits
```

__1)__ __a-__ On prendra comme entrée n = 25, combien de bits sont nécessaires pour représenter ce nombre en binaire ?    

__b-__ Dresser un tableau de suivi des valeurs des variables utilisées et vérifier la cohérence de la sortie.    

__2)__ Déclarer une fonction `nombre_bits` acceptant comme paramètre un nombre entier naturel `n` exprimé en base 10 et implémentez  l'algorithme précédent en Python.


# Exercice n°6 :  Ecriture d'un algorithme &#x1F3C6; &#x1F3C6; 

Ecrire un algorithme utilisant une boucle Tant Que permettant d’afficher les carres inférieurs ou égaux à un entier donné puis montrer que la boucle se termine en utilisant un variant.


# Exercice n°7 :  Algorithme de changement de base 🚀 

__1)__ En utilisant le pseudo-code fourni en cours, essayer d'implémenter en Python l'algorihme d'__écriture en base b d’un entier naturel n__ vu en cours. 

* On définira une fonction nommée `n_to_baseb` acceptant deux paramètres `b` et `n`  
* La traduction du pseudo-code est assez implicite sauf pour l'étape 5 :

```python
5:     ai ← chiffre correspondant à r
```
__Utiliser une variable `a`de type chaine de caractères pour représenter tous les ai. En concaténant les valeurs de r on obtiendra la représentation souhaitée__.

```python
>>> n_to_baseb(2, 10)
'1010'
```

__2)__ Quelle est la limite d'utilisation de cette fonction ? (Essayez avec plusieurs bases)
