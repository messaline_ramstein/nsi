function afficherTexte(texte) {
    var affichage = document.getElementById('affichage');
    affichage.innerHTML = '<p>'+texte+'</p>' + affichage.innerHTML ;
}
function bonjour() {
    var nom = document.getElementById('nom');
    afficherTexte('Bonjour ' + nom.value);
}
function changer_fond() {
    var liste = document.getElementById('liste_couleurs');
    var body  = document.body;
    body.style.backgroundColor = liste.value;
}
function curseur_change() {
    var curseur = document.getElementById('curseur');
    afficherTexte('Le curseur est maintenant à ' + curseur.value);
}
function verifierFormulaire() {
    var email = document.getElementById('email');
    if (email.value.includes('@')) {
        afficherTexte('L\'email est de longueur ' + email.value.length);
    }
    var bouton_envoyer = document.getElementById('envoyer');
    bouton_envoyer.disabled = false;
}
