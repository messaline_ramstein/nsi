# CHAP V. BOUCLES BORNEES                 
 TP

## Exercice 1 - Tours de chauffe 🏆 
Dans l'exercice suivant, on utilisera les boucles POUR décrites en cours pour réaliser les programmes suivants. __Il  faut encapsuler chaque programme dans une fonction.__

1) Ecrire une programme qui demande à l'utilisateur combien de fois il souhaite voir afficher la chaîne de caractères 'NSI' puis qui l'affiche n fois.

2) Ecrire un programme qui affiche les 20 premiers nombres pairs. 

3) Ecrire un programme qui affiche les entiers de 0 à 50 non compris de trois en trois séparés par le caractère ';'

4) 🥇 En mathématiques, une suite géométrique est une suite de nombres dans laquelle chaque terme permet de déduire le suivant par multiplication par un facteur constant appelé raison.

Ainsi, une suite géométrique a la forme suivante : __a,aq,aq2,aq3,aq4,…__

Par exemple : 2,16,128,1024,8192,… est une suite géométrique croissante, de premier terme a=2 de raison q=8 ( ici seuls les 5 premiers termes sont présentés)
Créer un programme demandant à l’utilisateur :

    • le premier terme a de la suite 

    • La raison q : de la suite 

    • le nombre n de termes à afficher. 
Le but sera d’afficher les n premiers termes de la suite sur une même ligne.

5) Ecrire une fonction affichant la table de multiplication d’un chiffre entré en paramètre.

## Exercice 2 - Dessiner avec la tortue : les déplacements 🏆 
La module ``turtle`` intégré à la bibliothèque standard de python permet de réaliser des dessins géométriques à l'aide d'une tortue virtuelle. Après avoir présenté quelques fonctionnalités de base, nous utiliserons ce module pour illustrer les boucles bornées vues en cours.
![alt text](image.png)

1) Tester l'exemple suivant :
```python
from turtle import *
forward(100)
left(90)
forward(100)
right(90)
forward(50)
right(90)
forward(150)
```
a) Expliquer la première ligne en rajoutant un commentaire.

Remarques :

• La tortue commence toujours au point de coordonnées (0,0) situé au centre de la fenêtre Python Turtle Graphics. 

• Pour réaliser un nouveau dessin, fermer d'abord cette fenêtre sinon la tortue repartira de l'état final précédent. 

__Attention : le nom du script enregistré ne doit pas être turtle.py !! Car c'est aussi le nom du module importé__

b) Définir une procédure ``carre()`` permettant de dessiner un carré et acceptant un paramètre ``a`` de type entier représentant la longueur de l'arête. Coder cette fonction d'abord sans boucle puis avec une boucle ``for``

c) À l'aide de cette procédure ``carre()`` définir une procédure nommée ``carres_tournants(n)`` qui dessine n carrés de côté 100 pivotant autour d'un sommet commun pour faire un tour complet. La figure suivante montre l'exemple pour n=7.

![alt text](image-1.png)

2) On peut également contrôler le crayon à l'aide de plusieurs instructions:
![alt text](image-2.png)

Tester le code suivant:

```python
forward(100)
up()
forward(50)
down()
forward(100)
```
En utilisant les fonctions précédentes et une boucle bornée, définir les procédures suivantes :

a) ``trois_carres(a)`` dessinant trois carres d'arête ``a`` alignés horizontalement et séparés d'une distance ``a``

b) ``grille_horizontale(x, a)`` dessinant x carres accolés horizontalement.

Conseils:

• utiliser des petites distances de l'ordre de 10 à 20 pixels, et peu d'alternances pour que le dessin "reste" dans la fenêtre 

•  régler la vitesse de la tortue à l'aide de la méthode ``speed`` (https://docs.python.org/3.3/library/turtle.html?highlight=turtle#turtle.speed) 

c) ``grille_verticale(y, a)`` dessinant y carres accolés verticalement.

d) ``grille(x, y, a)`` dessinant une grille de ``x`` sur ``y`` carrés

## Exercice 3 - Utilisation d'un accumulateur 🏆 🏆 

1) Définir une procédure ``spirale_carre()`` permettant de réaliser le dessin ci-dessous.
A chaque tour le trait est plus long de 10 pixels : il y a 20 traits au total. (vous pouvez ultérieurement modifier ces paramètres ou bien les demander à l'utilisateur)![alt text](image-3.png)

2) Le style du trait réalisé par le crayon peut être modifié :
![alt text](image-4.png)

a) Tester le code suivant:
```python
forward(100)
up()
forward(50)
down()
color("red")
width(5)
forward(100)
up()
forward (50)
down()
color("green")
width(10)
forward(50)
```
b) Définir une procédure ``faisceau(c)`` acceptant comme paramètre ``c`` une couleur sous forme de chaîne de caractères et permettant de réaliser un trait de plus en plus large de couleur choisie par l'utilisateur.

![alt text](image-5.png)
