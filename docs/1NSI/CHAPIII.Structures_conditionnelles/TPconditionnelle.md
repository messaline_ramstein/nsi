# CHAP III. OPERATEURS BOOLEENS ET STRUCTURES CONDITIONNELLES  
## TP 

<b> Exercice n°1 : Tables de vérité 🏆 </b>
1) En considérant deux propositions notées P et Q, dresser les tables de vérité correspondants aux expressions suivantes:

a) (¬P)∧Q

b) (¬P)∨Q

c) (¬P)∧(¬Q)

2) Donner les expressions booléennes python équivalentes


En construisant les tables de vérité correspondantes, montrer que :

a) (¬P)∧(¬Q)=¬(P ∨ Q)

b)(¬P)∨(¬Q)=¬(P ∧Q)

<b> Exercice n°2 : Expressions booléennes 🏆 </b>

Evaluer les expressions suivantes dans le Shell et expliquer leur résultat:

![alt text](image.png)

<b> Exercice n°3 : QCM Structures conditionnelles 🏆 </b>

Pour chaque cas, indiquer quelle est la valeur de x après exécution du code fourni. Une seule réponse est correcte (l'utilisation de l'ordinateur est interdite).

<i> Question 1 </i>

```python
x = 2

if x >= 2:

    x = x + 1

else:

    x = x - 1
```


Entourer la bonne réponse : 1 - 2 - 3 - 4

<i> Question 2 </i> 

```python
x = 4

if x < 5:

    x = x % 2

else:

    x = 2

if x == 0:

    x = 1

else:

    x = 2
````
Entourer la bonne réponse : 1 - 2 - 3 - 4

<i> Question 3 </i>

```python
x = 1

y = 2

if x < y and x > 2:

    x = 3

elif x > y or x >= 1:

    x = 2

else:

    x = 0
```
Entourer la bonne réponse : 0 - 1 - 2 - 3

<i> Question 4 </i>

```python
x = 0

if x < 2:

    x = x + 1

if x < 3:

    x = x * 4

if x < 4:

    x = x + 1
```

Entourer la bonne réponse : 0 - 1 - 4 - 5

<b> Exercice n°4 : Predicats 🏆 🏆 </b>

<i>💾 Un<b> predicat </b>est une fonction dont la valeur de retour est un booléen.
Par exemple, la fonction suivante is_positive() renverra True si l'argument passé en paramètre est positif, False dans le cas contraire. </i>

``def is_positive(n):``

    return n > 0

$>>>$ is_positive(12)

True

$>>>$ is_positive(-5)

False

<u>Remarques </u> :

    • dans le cas présenté, aucune précaution n'est prise pour vérifier que le paramètre est bien un nombre ! Ce n'est pas l'objectif de l'exercice. 

    • 💾 L'instruction return stoppe immédiatement le déroulement d'une fonction et donc de toutes les autres instructions dans cette fonction. 

Ecrire les prédicats suivants et les tester:

1) Définir un prédicat ``est_nul`` renvoyant ``True`` si le nombre passé en paramètre est égal à zero, ``False`` sinon.

2) Définir un prédicat ``est_obtus`` renvoyant ``True`` si l'angle exprimé en degrés passé en paramètre est obtus, ``False `` sinon. (On considérera uniquement les angles compris entre 0 et 180°)

3) Définir un prédicat ``est_voyelle`` renvoyant ``True`` si le caractère passé en paramètre est une voyelle (peut importe la casse) , ``False`` sinon.

4)
a- Définir un prédicat ``is_liquid_water`` renvoyant ``True`` si le nombre passé en paramètre est une température exprimée en °C à laquelle l'eau est liquide, ``False`` sinon(on considérera une pression atmosphérique normale).

b- Proposer une autre version de ce prédicat ``is_liquid_water_bis`` sans utiliser d'opérateur logique (and, or, not ...) mais avec uniquement des suites d'instructions conditionnelles.

5) Définir un prédicat ``is_even`` renvoyant ``True`` si le nombre passé en paramètre est pair,  ``False`` sinon.

6) 🥇 Depuis l'ajustement du calendrier grégorien en 1582, les années sont bissextiles si l'année est divisible par 4 sans l’être par 100 ou encore si l'année est divisible par 400
Définir un prédicat est_bissextile  renvoyant ``True`` si l'entier passé en paramètre est une année bissextile et  ``False``sinon.

<i> Indications : </i>

    • L'année doit être postérieure à 1582, tester d'abord ce critère dans votre Predicat. 

    • Quelques années bissextiles à tester : 2012, 2016, 2020,2024 et quelques années ne l'étant pas : 1900, 2100


<b> Exercice n°5 : Machine à sous 🏆 🏆 </b>

Il existe différents modèles de machines à sous, aussi appelées bandits manchots.
Un modèle simple consiste en 3 rouleaux comportant 5 symboles (représentés ici par les chiffres de 1 à 5).
 Les combinaisons gagnantes et les gains correspondants sont indiqués ci-dessous :
 ![alt text](image-1.png)


Définir une fonction ``calcul_gain()``  dont la valeur de retour est la somme gagnée.  Il faudra importer une fonction du module random.