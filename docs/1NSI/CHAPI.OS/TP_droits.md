# CHAP I.SYSTEMES D’EXPLOITATION                           
__TP  Utilisateurs et droits des fichiers (UNIX)__

## Exercice 1
Ci-dessous, le résultat de la commande ls -l en console :

![alt text](image-4.png)

    1. Quel est l’ utilisateur ?
    2. Quel est le nom de la machine utilisée ?
    3. Quel est le répertoire courant ?
    4. Combien contient-il de fichiers ? de dossiers ?(non cachés)
    5. Pour chaque fichier ou dossier, expliquer chacune des informations affichées.
    6. Donner la valeur octale des permissions sur ces fichiers / dossiers.

##      Exercice 2
A quels droits correspondent les valeurs octales suivantes : 451 ; 742 ; 254 et 650 ?

## Exercice 3
Quels sont les droits sur un fichier et les valeurs octales correspondants à :

    • Le propriétaire peut afficher le contenu et le modifier.

    • Le groupe propriétaire peut lire le contenu.

    • Les autres peuvent exécuter le fichier.

## Exercice 4
Quels sont les droits sur un dossier et les valeurs octales correspondants à:

    • Le propriétaire peut lister le contenu et créer / supprimer des fichiers.

    • Le groupe propriétaire lister le contenu uniquement.

    • Les autres peuvent traverser le dossier uniquement.

##                Exercice 5
Chercher à comprendre chaque étape, à partir de la question 1, en affichant les droits des fichiers et répertoires. Au préalable…

• Dans votre session utilisateur,en utilisant l’explorateur de fichiers, créer un dossier __NSI__, puis y créer  un dossier __TD_exo5__ et y déposer le fichier [frozen-bubble-2.2.0.zip](./frozen-bubble-2.2.0.zip)
• Ouvrir un terminal et se placer  dans le dossier __TD_exo5__.
• Dézipper le fichier précédent à l'aide de la commande : ``unzip frozen-bubble-2.2.0.zip``

• Se placer dans le répertoire __frozen-bubble-2.2.0/__ et exécuter la commande : ``bash first.sh``

__Questions__

1. Lister les fichiers et répertoires contenus dans le dossier courant avec la commande ``ls -l``

2. Essayer d'afficher le contenu du répertoire __tools__. Que se passe-t-il ?

3. Essayer de se placer dans le répertoire __icons__. Peut-on lister son contenu ?

4. Essayer de créer un fichier __i_was_here.txt__ dans le répertoire __server__. 

5. Essayer d'afficher le contenu du fichier __README__ avec la commande ``cat``. Que se passe-t-il ?

6. Ouvrir le fichier __settings.mk__ avec un éditeur de texte geany en tapant ``geany settings.mk`` et tenter de le modifier puis de le sauvegarder. Est-ce possible ?

7. Afficher le contenu du fichier __first.sh__ avec la commande ``cat``. Ce fichier est celui qui a été exécuté au préalable : les commandes qu'il contient sont celles qui ont supprimé des droits sur les fichiers et répertoires manipulés précédemment. Vérifier que cela correspond bien à ce qu’on a obtenu dans les questions précédentes.

## Exercice 6

• Dans le terminal, dans votre dossier personnel __NSI__, créer un dossier __TD_exo6__ puis un fichier __fich1__ vide.

• Observer les droits de __fich1__,

• Lui attribuer  tous les droits pour tout le monde à l'aide de ``chmod`` et observer à nouveau ses droits

• Créer un fichier ``private.txt`` dans __TD_exo6__ avec les permissions suivantes :

        ◦ Lecture et écriture seulement autorisées pour le propriétaire du fichier
        ◦ Aucun droit pour les autres.
        ◦ Ajouter au groupe propriétaire la possibilité de lire le fichier. 
◦ Modifier les droits d'accès de __private.txt__ pour en empêcher la lecture, l'écriture et l'exécution par qui que ce soit. Essayer de lire le contenu du fichier. Que se passe-t-il ?

◦ Ajouter la possibilité au propriétaire de lire le fichier. Essayer de modifier le contenu du fichier __private.txt__. Quelle commande utiliser pour avoir le droit de modifier le contenu du fichier ?

## Exercice 7

• Créer un fichier __mon_prog.sh__ dans un dossier __TD_exo7__

• À l'aide de l’éditeur __Geany__, copier le code suivant à l'intérieur et l’enregistrer dans le fichier créé précédemment :
![alt text](image-5.png)


Exécuter le script précédent en saisissant (en ligne de commande) : ``./monprog.sh``
    • Que se passe-t-il ?


    • Comment y remédier ?