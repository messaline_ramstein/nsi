# CHAP I.SYSTEMES D’EXPLOITATION                                              
__Partie 2 : Utilisateurs et droits des fichiers__

## I.Utilisateurs

Les systèmes de type "UNIX" sont des systèmes multi-utilisateurs. Plusieurs utilisateurs peuvent donc partager un même ordinateur, chaque utilisateur possédant un identifiant de connexion et un environnement de travail qui lui est propre.
On distingue 2 grands types de comptes utilisateur :

 • Les __comptes des usagers__ sont les comptes d'utilisateur qui sont attribués à des usagers physiques de votre ordinateur. Ils doivent être créés pour chacun des usagers de votre ordinateur. C'est ce type de compte qui identifie les personnes qui se servent de votre ordinateur ;

 • Le __compte super-utilisateur__ est un compte unique qui dispose de toutes les autorisations. Classiquement, il sert essentiellement à l'administration de votre système. Son identifiant est root, d'où son appellation fréquente de compte root.
Au lieu de gérer les utilisateurs un par un, il est possible de créer des __groupes d'utilisateurs__. L'administrateur attribue des droits à un groupe au lieu d'attribuer des droits particuliers à chaque utilisateur. Chaque utilisateur appartient donc à un groupe principal et à des groupes secondaires.
Chaque utilisateur possède des droits qui lui ont été octroyés par le "super utilisateur".

 Nous nous intéresserons ici uniquement aux __droits liés aux fichiers et aux dossiers__, mais vous devez savoir qu'il existe d'autres droits liés aux autres éléments du système d'exploitation (imprimante, installation de logiciels...)
 
 ## II.UID et GID

À l'identifiant de connexion d'un utilisateur correspond un identifiant numérique unique (par exemple 1001), nommé __UID__ pour l'anglais User IDentifier. En interne, le système d'exploitation utilise exclusivement l'UID.
L'identifiant numérique du groupe principal d'un utilisateur est nommé __GID__ pour l'anglais Group IDentifier.
La commande ``id`` dans un terminal permet d'afficher les identifiants numériques et les groupes de l'utilisateur courant.

## III.Types de droits

Il y a 3 catégories d'utilisateurs d'un fichier :

 • l'__utilisateur propriétaire__ du fichier ou user (u)

• Le __groupe propriétaire__ (g)

• Les __autres ou others__ (o)

Les « permissions » accordées à chaque utilisateur ou groupe d'utilisateurs sont de 3 types :

 • La __lecture__ : Read (r)

 • L'__écriture__ : Write (w)

• L’__exécution__ : eXecute (x)

La commande ``ls -l``  permet de lister le contenu du répertoire courant et d'afficher de nombreuses informations sur les fichiers de ce répertoire. 
 
Expliquons la visualisation des droits sur un exemple :
![](image.png)
![alt text](image-1.png)

_première ligne_ :

    • __d__ : cette ligne concerne un dossier  ``cours_maths``

    • __rwx__ : l’utilisateur propriétaire lucasr a le droit de lire, modifier et accéder à ce dossier.

    • __r-x__ :le groupe propriétaire 12NSI1  a le droit de voir le dossier et d’y accéder  mais pas d’en modifier le contenu.

    • __r-x__ : les autres utilisateurs ont le droit de voir le dossier et d’y accéder mais pas d’en modifier le contenu

    • __512__ : taille du dossier en octets

_deuxième ligne_ :

• __-__ : cette ligne concerne un fichier ``test.txt``

• rw- : l’utilisateur propriétaire lucasr a le droit de lire et de modifier le fichier mais pas de l’exécuter

• r-- : le groupe propriétaire 12NSI1  a le droit de lire le fichier mais pas de le modifier ni de l’exécuter

• r-- : les autres ont le droit de lire le fichier mais pas de le modifier ni de l’exécuter

• 7 : taille du fichier en octets

##      IV. Changer les droits

Seuls le super-utilisateur du système (généralement __root__) et le propriétaire d'un fichier peuvent changer ses permissions d'accès. Pour cela, on peut utiliser la commande ``chmod``(abréviation de __change mode__).

_Méthode 1 : en utilisant l'écriture octale_

• Attribuer des droits de lecture et écriture au propriétaire seul sur le fichier de nom __fichier1__ :

![alt text](image-2.png)

_600 correspond à 400+200 (soit à rw- --- ---)_.

• Attribuer tous les droits au propriétaire et aucun au groupe propriétaire et aux autres sur __/home/cedric/NSI__ et tout ce qu'il contient :
![alt text](image-3.png)

_Méthode 2 : En utilisant u,g,o, +,-,=_

Il existe un autre moyen de modifier les droits d'un fichier avec la commande ``chmod``.
Dans ce mode d'utilisation, il faut se rappeler que :

• __u__ signifie : user (propriétaire) ;

• __g__ signifie : group (groupe) ;

• __o__ signifie : other (autres) ;

... et que :

• __+__ signifie : « Ajouter le droit » ;

• __-__ signifie : « Supprimer le droit » ;

• __=__ signifie : « Affecter le droit ».

Ajouter le droit d'écriture au groupe sur __unfichier__ :
``chmod g+w unfichier``

Enlever le droit de lecture aux autres sur __unfichier__ : 
``chmod o-r unfichier``

Ajouter les droits de lecture et d'exécution au propriétaire sur __unfichier__ :      
``chmod u+rx unfichier``

Ajouter le droit d'écriture au groupe et l'enlever aux autres sur __unfichier__:
``chmod g+w,o-w``

Affecter tous les droits au propriétaire, juste la lecture au groupe, rien aux autres sur __unfichier__:
``chmod u=rwx,g=r,o=-``

## V.Changer les propriétaires

Les commandes ``chown`` (change owner) et ``chgrp`` (change group) sont à exécuter en tant que __root__ pour changer le propriétaire ou le groupe propriétaire d'un fichier.

Changer le propriétaire de __unfichier__ :
``chown nouvel_utilisateur unfichier``

Changer le groupe propriétaire de __unfichier__ : 
``chgrp nouveau_groupe unfichier``

Changer le propriétaire et/ou le groupe propriétaire de __unfichier__ : 
``chown nouvel_utilisateir:nouveau_groupe unfichier``

_Pour un dossier, la syntaxe est la même mais il faut l’attribut -R pour la récursivité_
