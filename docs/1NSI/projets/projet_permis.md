# Projet N°1  NSI   Gestion du solde du permis de conduire
Pour le 2 Novembre 2024

Vous allez devoir créer une application de gestion du permis à points dans un fichier ``permis_nom.py``. Lire :

https://www.service-public.fr/particuliers/vosdroits/F31551

1. Créer 3 variables contenant chacune le prénom d’une personne. Créer , pour chacune, la variable correspondant à son nombre de points initialisé à 12. 

2. Créer une procédure  ``affiche_infractions()`` qui affiche la liste numérotée de 1 à 6 des infractions liées au non respect des limitations de vitesse.

3. Créer une fonction ``calcul_solde(nbpoints,infraction)`` qui calcule et retourne le nouveau solde après une infraction en fonction de l’infraction commise et du nombre de points avant l’infraction. Le programme devra alors retirer le nombre de points correspondant, donner le solde restant. Celui-ci ne peut  pas être négatif.

4. Créer une procédure ``afficher_solde(nbpoints,personne)``qui  affiche le nombre de points restants de la personne  concernée. /3

5. Créer la fonction principale ``simuler()``.
Cette fonction doit :

• permettre à l’utilisateur de choisir une personne parmi les 3 (grâce à ``input()``)

• de lui associer son nombre de points 

• de lui calculer puis de lui afficher le solde du permis tant que la personne répond __« oui »__ à la question __« Souhaitez-vous saisir une nouvelle infraction ? »__


_Remarque : Le programme ne devra pas demander de saisir une infraction si le solde est à 0. Dans ce cas la phrase __Le solde du permis de conduire de XXX est nul__._

Barême

• ``afficher_infractions()`` –> Affiche les 6 infractions et leur numéro          /2

• ``calcul_solde(nbpoints,infraction)`` –> Calcule et retourne le nouveau solde après une infraction en fonction du nombre de points avant l’infraction            /3

• ``afficher_solde(nbpoints,personne)``  –>  Affiche le nombre de points restants de la personne  concernée /3

• ``simuler()`` → programme principal : j1 est la personne  N°1 et nbj1 est le nombre de points restant sur son permis
Cette fonction doit :

    • permettre à l’utilisateur de choisir une personne /1
    • de lui associer son nombre de points /2
    • de lui calculer puis de lui afficher le solde du permis tant que la personne répond « oui »:/6


_Nom de fichier : /1_             

_documentation des fonctions:/1_   
 
_clarté du programme :/1_