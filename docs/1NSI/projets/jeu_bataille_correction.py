#---------------------------------------------
#  Importation des modules
#---------------------------------------------

import random
import time

#----------------------------------------------
# Fonctions création jeu de cartes 
#----------------------------------------------

def jeu_de_cartes():
    """Fonction qui renvoie une liste comportant un jeu de 52 cartes."""
    return ["As ♠", "2 ♠", "3 ♠", "4 ♠"
                    , "5 ♠", "6 ♠", "7 ♠", "8 ♠"
                    , "9 ♠", "10 ♠", "Valet ♠"
                    , "Dame ♠", "Roi ♠", "As ♣", "2 ♣"
                    , "3 ♣", "4 ♣", "5 ♣", "6 ♣"
                    , "7 ♣", "8 ♣", "9 ♣", "10 ♣"
                    , "Valet ♣", "Dame ♣", "Roi ♣"
                    , "As ♥", "2 ♥", "3 ♥", "4 ♥"
                    , "5 ♥", "6 ♥", "7 ♥", "8 ♥"
                    , "9 ♥", "10 ♥", "Valet ♥"
                    , "Dame ♥", "Roi ♥", "As ♦"
                    , "2 ♦", "3 ♦", "4 ♦"
                    , "5 ♦", "6 ♦", "7 ♦", "8 ♦"
                    , "9 ♦", "10 ♦", "Valet ♦"
                    , "Dame ♦", "Roi ♦"]


#----------------------------------------------
# Fonctions d'affichage
#----------------------------------------------

def afficher_carte(jeu1, jeu2):#0.5
    """Procédure qui affiche les cartes des joueurs
    :param jeu1: jeu du joueur1
    :type jeu1:list[str]
    :param jeu2: jeu du joueur2
    :type jeu2: list[str]
    :return: Les premières cartes des jeux 1 et 2
    :rtype: str"""
    print(jeu1[0], jeu2[0])


def afficher_nombres_cartes(jeu1, jeu2, nom1, nom2):#1
    """Procédure qui affiche le nombre de cartes par jeu de joueur
    :param jeu1: jeu du joueur1
    :type jeu1:list[str]
    :param jeu2: jeu du joueur2
    :type jeu2: list[str]
    :return: None"""
    print(nom1, "a un paquet de ", len(jeu1), " cartes")
    print(nom2, "a un paquet de ", len(jeu2), " cartes")


def afficher_vainqueur_tour(gagnant, nom1, nom2):#1.5
    """Procédure qui affiche le vainqueur du tour
    :param gagnant: gagnant du tour
    :type gagnant:str
    :param nom1: nom du joueur 1
    :type nom1: str
    :param nom2: nom du joueur 2
    :type nom2: str
    :return:None"""
    if gagnant == "joueur1":
        print(nom1, "a gagné le tour.")
    elif gagnant == "joueur2":
        print(nom2, "a gagné le tour.")
    else:
        print("Bataille ! ")


def afficher_vainqueur_jeu(gagnant, nom1, nom2):#1
    """Procédure qui affiche le vainqueur du jeu
    :param gagnant: gagnant du jeu
    :type gagnant:str
    :param nom1: nom du joueur 1
    :type nom1: str
    :param nom2: nom du joueur 2
    :type nom2: str
    :return:None"""
    if gagnant == "joueur1":
        print(nom1, "a gagné le jeu.")
    else:
        print(nom2, "a gagné le jeu.")


#----------------------------------------------
# Fonctions relatives aux cartes des joueurs
#-----------------------------------------------

def valeur_carte(carte):#1.5
    """ Fonction qui attribue une valeur à une carte.
    :param carte_joueur1: une carte du jeu de 52 cartes
    :type carte_joueur1: str
    :param carte_joueur2: une autre carte du jeu de 52 cartes
    :type carte_joueur2: str
    :return: Lequel des joueurs a la carte qui a le plus de valeur
    :rtype: str"""
    liste_valeur= ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Valet', 'Dame', 'Roi', 'As']
    return liste_valeur.index(carte[0:carte.index(" ")])+2

#  Deuxième possibilité
# 
# def valeur_carte(carte):
#     i_space= carte.find(" ")
#     valeur = carte[:i_space]
#     if valeur == 'As':
#         v = 14
#     elif valeur == 'Roi':
#         v = 13
#     elif valeur == 'Dame':
#         v = 12
#     elif valeur == 'Valet':
#         v = 11
#     else:
#         v = int(valeur)
#     return v


def comparer_carte(carte_joueur1, carte_joueur2):#1
    """ Fonction qui compare les valeurs des cartes des joueurs et retourne le vainqueur ou bataille.
    :param jeu_complet: jeu de 52 cartes
    :type jeu_complet: list[str]
    :param nombre_cartes: nombre de cartes souhaitées dans le jeu de carte
    :type nombre_cartes: int
    :return: le nouveau jeu de carte
    :rtype: list"""
    if valeur_carte(carte_joueur1)>valeur_carte(carte_joueur2):
        return 'joueur1'
    elif valeur_carte(carte_joueur1)<valeur_carte(carte_joueur2):
        return 'joueur2'
    else:
        return 'bataille'


#----------------------------------------------
# Fonctions relatives aux jeux des joueurs
#-----------------------------------------------

def distribution_jeu(jeu_complet, nombre_cartes):#1.5
    """Fonction qui crée les jeux des joueurs.
    :param jeu_complet: jeu de 52 cartes
    :type jeu_complet: list
    :param nombre_cartes: nombre de cartes souhaitées dans le jeu de carte
    :type nombre_cartes: int
    :return: le nouveau jeu de carte
    :rtype: list"""
    jeu_joueur = []
    for _ in range(nombre_cartes):
        nombre_aleatoire = random.randint(0,len(jeu_complet)-1)
        jeu_joueur.append(jeu_complet[nombre_aleatoire])
        del(jeu_complet[nombre_aleatoire])     
    return jeu_joueur

def ordonner_jeu(jeu_vainqueur, jeu_perdant):#1.5
    """Procedure qui place les cartes dans les jeux après le tour.
    :param jeu_vainqueur: jeu du vainqueur
    :type jeu_vainqueur: list
    :param jeu_perdant: jeu du perdant
    :type jeu_perdant: list"""
    jeu_vainqueur.append(jeu_perdant[0])
    jeu_vainqueur.append(jeu_vainqueur[0])       
    del(jeu_perdant[0])
    del(jeu_vainqueur[0])


def gestion_bataille(jeu_joueur1, jeu_joueur2, gagnant_bataille):#2
    """ Procédure qui gère les batailles.
    :param jeu_joueur1: jeu du joueur 1
    :type jeu_joueur1: list
    :param jeu_joueur2: jeu du joueur 2
    :type jeu_joueur2: list
    :param gagnant_bataille: paramètre qui fluctue au fur et à mesure du déroulement de la fonction, sa valeur initiale sera toujours 'bataille !'
    :type gagnant_bataille: str"""
    compteur = 0
    while gagnant_bataille == "bataille":
        compteur = compteur + 2
        if (compteur > len(jeu_joueur1) - 1 and len(jeu_joueur1)<len(jeu_joueur2)):
            compteur = len(jeu_joueur1)-1
            gagnant_bataille = "joueur2"
        elif compteur > len(jeu_joueur2) - 1:
            compteur = len(jeu_joueur2)-1
            gagnant_bataille = "joueur1"
        else:
            gagnant_bataille = comparer_carte(jeu_joueur1[compteur], jeu_joueur2[compteur])
    for i in range(compteur+1):
        gestion_tour(gagnant_bataille, jeu_joueur1, jeu_joueur2)

def gestion_tour(gagnant, jeu_joueur1, jeu_joueur2):#2
    """ Procédure qui gère les trois cas de figure rencontrés dans un tour.
    :param gagnant: vainqueur du tour
    :type gagnant: str
    :param jeu_joueur1: jeu du joueur 1
    :type jeu_joueur1: list[str]
    :param jeu_joueur2: jeu du joueur 2
    :type jeu_joueur2: list"""
    if gagnant == 'joueur1':
        ordonner_jeu(jeu_joueur1, jeu_joueur2)      
    elif gagnant == 'joueur2':
        ordonner_jeu(jeu_joueur2, jeu_joueur1)   
    else:
        gestion_bataille(jeu_joueur1, jeu_joueur2, gagnant)
        
        
#---------------------------------------------
# Fonction principale
#---------------------------------------------
       
def jeu():#6.5
    """Procédure principale du jeu qui pilote l ensemble des fonctions."""
    jeu_complet = jeu_de_cartes()
    nom_joueur1 = input("Entrer le nom du joueur 1: ")
    nom_joueur2 = input("Entrer le nom du joueur 2: ")
    jeu_joueur1 = distribution_jeu(jeu_complet, 26)
    jeu_joueur2 = jeu_complet
    while ((len(jeu_joueur1)>0) and (len(jeu_joueur2)>0)):
        vainqueur = comparer_carte(jeu_joueur1[0], jeu_joueur2[0])
        afficher_nombres_cartes(jeu_joueur1, jeu_joueur2, nom_joueur1, nom_joueur2)
        afficher_carte(jeu_joueur1,jeu_joueur2)
        afficher_vainqueur_tour(vainqueur, nom_joueur1, nom_joueur2)
        print("")
        gestion_tour(vainqueur, jeu_joueur1, jeu_joueur2)
        time.sleep(0.5)
    if len(jeu_joueur1) != 0:
        vainqueur = "joueur1"
    afficher_vainqueur_jeu(vainqueur, nom_joueur1, nom_joueur2)
    
jeu()