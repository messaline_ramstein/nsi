# CHAP II. DECOUVERTE PYTHON                                        TP2.FONCTIONS ET PROCEDURES

<b> Exercice n°1 : Fonctions natives et documentation 🏆 </b>

1) Faire apparaître la documentation de la fonction ``pow()``

a- A quoi sert-elle ? La tester avec un exemple de votre choix pour le vérifier.
Remarque: le symbole ``/`` dans la documentation des paramètres (...,/) n'est pas à prendre en compte.

b- Combien de paramètres au minimum doivent être utilisés au minimum ? Et au maximum?

2) Faire apparaître la documentation de la fonction ``abs()``

a- A quoi sert-elle ? La tester avec un exemple de votre choix pour le vérifier.

b- Combien de paramètre(s) doivent être utilisés ?

<b> Exercice n°2 : Quelques fonctions personnalisées 🏆 </b>

Pour cet exercice, créer préalablement un script nommé ``fonctions_persos.py``. Pour chacun des cas suivants on définira correctement une fonction en n'oubliant pas de :

    * choisir un nombre de paramètres adaptés 
    * nommer les fonctions et les paramètres de manière non ambigüe 
    * de créer une docstring associée et de tester. 

1) Définir une fonction renvoyant le carré d'un nombre
2) Définir une fonction renvoyant le périmètre d'un rectangle
3) Définir une fonction renvoyant la moyenne de 3 notes
4) 🥇Définir une fonction retournant la <b>moyenne pondérée </b> de 2 notes

<b> Exercice n°3 : IMC 🏆  </b>

1) Créer un nouveau script nommé ``imc.py``.
2) En reprenant la définition vue lors de la séance précédente indiquer combien de paramètres sont nécessaires à la définition d'une fonction permettant de calculer l'IMC.

3) Créer la fonction ``calcul_imc`` correspondante en choisissant des noms de paramètres explicites.
On n’oubliera  pas de renseigner la <b>docstring </b>.
4) Après avoir sauvegardé votre script, tester la fonction avec les exemples suivants :
```python
>>> calcul_imc(95, 1.81)

28.997893837184456

>>> calcul_imc(140, 2.04)

33.64090734332949
```
5) On souhaite améliorer cette fonction en donnant une valeur arrondie au dixième du résultat. Ecrire une fonction ``calcul_imc_bis``en utilisant la fonction ``round`` vue en cours (reprendre la documentation associée si nécessaire)
```python
>>> calcul_imc_bis(140, 2.04)

33.6
```

<b> Exercice n°4 : Variables et espaces de noms 🏆 </b>

Soit le script suivant :

```python
x = 3

def incremente(a) :

        x=5
        a = a + 1
        a = a + 1
        return a

b=incremente(x)

print(b)

print(x)
```


Visualiser l'exécution du code étape par étape à l'aide Python Tutor :
http://www.pythontutor.com/visualize.html#mode=edit
1) Combien de variables nommées  sont utilisées dans ce programme ? Lesquelles sont <b>locales </b>? Lesquelles sont <b>globales </b>?


2) A la fin du programme, combien de variables nommées ``x `` existent encore en mémoire ?

<b> Exercice n°5 : Théorème de Pythagore 🏆 🏆 </b>

1) Ecrire la fonction ``calcul_hypotenuse`` qui acceptera deux paramètres notés ``a`` et ``b`` correspondant aux longueurs des 2 plus petits côtés d'un triangle rectangle (exprimées avec la même unité).
La valeur de retour de cette fonction sera la longueur de l'hypoténuse.

a) De quelle fonction du module ``math`` a-t-on besoin pour calculer la longueur de l'hypoténuse ?

b) Après avoir rédigé votre code, enregistrer le script sous le nom ``hypotenuse.p``y et vérifier votre fonction à l'aide des exemples suivants:

```python
>>> calcul_hypotenuse(2, 3)

3.605551275463989 
>>> calcul_hypotenuse(5, 2)

5.385164807134504
```

2) Le module math possède une fonction nommée ``hypot``. L’importer, puis rechercher dans sa documentation son mode d'utilisation.
Que constate-t-on ? ![alt text](image-4.png)


<b> Exercice n°6 : Des fonctions en série 🏆 🏆 </b>
1) Définir une fonction ``cube`` renvoyant le cube d'un nombre passé en paramètre.
2) On rappelle que le volume d'une sphère peut être calculé à l'aide de la formule suivante ​ $ {4*\pi*r^3}/3 $

a- Importer pi provenant de la bibliothèque ``math``. On peut évaluer sa valeur :
```python
>>> pi

3.141592653589793
```
b- Définir une fonction ``volume_sphere`` renvoyant le volume d'une sphère en fonction du rayon passé en paramètre.

3) Proposer une deuxième version de cette fonction ``volume_sphere_bis`` qui fait appel à la fonction ``cube`` initialement définie.

<b> Exercice n°7 : Lancer de dés 🏆 </b>

1) Beaucoup de jeu de rôles (JDR) utilisent des dés spéciaux avec un nombre de faces adapté.

Définir une fonction ``dice_20`` sans paramètre simulant le lancer d'un dé à 20 faces (le nombre le plus petit étant 1 et les valeurs allant de un en un )

remarque : lors de la définition d'une fonction sans paramètre ou de son appel les parenthèses sont quand  même obligatoires, par exemple :
```python
>>> dice_20():

15
```
2) Ajouter un commentaire expliquant l'importation réalisée.

3) Définir une fonction ``dices_roll`` (sans paramètre) simulant le lancer simultané de 3 dés 20.

Cette fonction renverra uniquement la somme du lancer : on utilisera bien évidemment la fonction précédente dans cette nouvelle fonction.