# CHAP II. DECOUVERTE PYTHON                                         TP1.Manipuler des nombres en Python

<b>Exercice n°1 : Opérateurs mathématiques </b>

Calculer les valeurs des expressions suivantes par vous même  puis vérifier dans le Shell.
En déduire, à chaque, fois les priorités lors d'évaluations des expressions contenant (), *, + , -, **, /.
![alt text](image.png)

<b> Conclusions sur les priorités opératoires en Python: </b>

..................................................................................................................................
.................................................................
.................................................................

<b> Exercice n°2 : Nombres entiers et à virgule flottante  </b>

1) Ecrire les nombres suivants dans le Shell (en respectant la syntaxe imposée par le langage). Les évaluer et noter leur type.

![alt text](image-1.png)

2) 🥇Taper les instructions suivantes dans le Shell:

```python
>>>import sys

>>>sys.float_info
```
On accède ainsi aux limitations du langage lors de la manipulation de nombres à virgule flottante.
a- Noter la valeur maximale pouvant être atteinte par un flottant en python : max :

b- Tester ce qui se passe au delà de cette valeur.

c- Que signifie l'indication renvoyée ?


<b>Exercice n°3 : Divisions 🏆 </b>

Evaluer les expressions suivantes d'abord sur papier puis vérifier en utilisant le Shell
```python
>>> 5 / 2

>>> 4 / 2

>>> 4 // 2

>>> 4 % 2

>>> 5 % 2

>>> 7 + 7 % 2
```


<b>Exercice n°4 : utilisation de variables 🏆🏆 </b>
1) Pour chacune des lignes de code suivantes indiquer l’erreur commise

``` python
>>> moyenne eleve = 10


>>> 12 = moyenne


>>> a = 3 + s


>>> y - 1 = 8


>>> if = 50

```

2) On souhaite réaliser une division euclidienne :

    • Déclarer deux variables nommées dividende et diviseur en leur attribuant respectivement les valeurs 15 et 3. 

    • Déclarer une variable quotient et établir son expression. 
    • Déclarer une variable reste et établir son expression. 
3) Assigner respectivement les valeurs 1, 5 , 9 aux trois variables x, y, z puis tester ensuite les expressions suivantes et vérifier leur valeur.

![alt text](image-2.png)

<b> Exercice n°5 : Choix des variables 🏆🏆 </b>
1) Proposer des noms de variables correspondants à ces situations et respectant les recommandations d'écriture:
    • Température mesurée dans la salle de classe 
    • nombre de poissons dans un aquarium 
    • nombre à deviner 

2) La fréquence d'un signal sonore est de 20kHz. Après avoir déclaré la variable adaptée à la situation. Donner l'expression de la période de ce signal (on utilisera bien évidemment une autre variable)

3) 🥇 L'indice de masse corporelle est une grandeur permettant d'estimer la corpulence d'une personne.
a- Après avoir consulté l'article de Wikipedia le présentant proposer un nombre de variables adaptées pour calculer cette grandeur.
b- Tester l'expression établie avec les couples de valeurs suivants correspondant à une partie des exemples donnés

![alt text](image-3.png)









c- Quel(s) inconvénient(s) trouve-t-on à utiliser le Shell et les variables précédentes? 