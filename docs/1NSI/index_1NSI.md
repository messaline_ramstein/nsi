# Cours de 1NSI Lycée Marguerite de flandre

[diaporama de présentation de la spécialité en 1ère](./pres1ere.odp)

## CHAP I. Les systèmes d'exploitation

[cours](./CHAPI.OS/cours_OSMR.odp)
[vidéo](./CHAPI.OS/le-systeme-dexploitation-en-trois-idees-cle.mp4)

[TP](./CHAPI.OS/TP_OS_linux.docx)

[cours droits](./CHAPI.OS/cours_droits.md)

[TP droits](./CHAPI.OS/TP_droits.md)

__fichier utile pour le TP droits:__
[frozen-bubble-2.2.0.zip](./CHAPI.OS/frozen-bubble-2.2.0.zip)

[correction DS1](./CHAPI.OS/DSOS_corr.odt)

## CHAP II.Démarrer en Python

[cours1.Manipuler des nombres](./CHAPII.Démarrer%20en%20Python/cours1_manipuler_nombres.md)
[TP1](./CHAPII.Démarrer%20en%20Python/TP1_nombres.md)

[cours2.Les Fonctions](./CHAPII.Démarrer%20en%20Python/cours2_fonctions.md)
[TP2](./CHAPII.Démarrer%20en%20Python/TP2_fonctions.md)

[cours3. Les chaînes de caractères](./CHAPII.Démarrer%20en%20Python/cours3_strings.md)
[TP3](./CHAPII.Démarrer%20en%20Python/tp3_strings.md)

## CHAP III. Structures conditionnelles

[cours](./CHAPIII.Structures_conditionnelles/cours_booleens_conditions.md)
[TP](./CHAPIII.Structures_conditionnelles/TPconditionnelle.md)

[exercices](./CHAPIII.Structures_conditionnelles/exosif.odt)

## CHAP IV. BOUCLE NON BORNEE

[cours](./CHAPIV.BOUCLE%20NON%20BORNEE/c4_while_algo.md)
[TP](./CHAPIV.BOUCLE%20NON%20BORNEE/tp_boucles_while.md)

## CHAP V. BOUCLE BORNEE

[cours](./CHAPV.BOUCLE%20BORNEE/cours_for.md)
[TP](./CHAPV.BOUCLE%20BORNEE/TP_for.md)

[exercices if/while/for](./CHAPV.BOUCLE%20BORNEE/exosif_while_for.odt)

[correction DS If/while/for](./CHAPV.BOUCLE%20BORNEE/DS_if_while_for_corr.odt)

## CHAP VI. REPRESENTATION DES ENTIERS EN MACHINE

[cours1.Les bases](./CHAPVI.Numération/cours1_bases.pdf)
[TP1](./CHAPVI.Numération/TP1.Bases.pdf)

[cours2.Les entiers relatifs](./CHAPVI.Numération/cours2_entiers_relatifs.pdf)
[TP2](./CHAPVI.Numération/TP2.Relatifs.pdf)

[video compte à rebours](./CHAPVI.Numération/cpt_a_rebours.mp4) &nbsp; [vidéo compteur mécanique](./CHAPVI.Numération/mechanical_odometer.mp4)

[exercices d'entraînement](./CHAPVI.Numération/exos_entiers.pdf)

##  CHAPVII. SPECIFICATIONS ET TESTS DES PROGRAMMES

[cours](./CHAPVII.spécfications_tests/docs_11_specification_tests_c11_specification_tests.pdf)
[TP](./CHAPVII.spécfications_tests/TPspecifications.md)

[correction DS Entiers et spécification des programmes](./CHAPVII.spécfications_tests/DSNum_spe%20-%20Copie.odt)

## CHAP VIII. LES LISTES

[introduction](./CHAPVIII.LISTES/intro.md)

[TP/cours](./CHAPVIII.LISTES/TPcourstableaux.odt)

[TP Parcours de listes](./CHAPVIII.LISTES/TPparcourseq.odt)

[TP Listes en compréhension](./CHAPVIII.LISTES/TPcompréhension.odt)

[TP jeu de cartes](./CHAPVIII.LISTES/jeudecartes.odt)

[coreection DS Listes](./CHAPVIII.LISTES/DSsequences%20Corr.odt)

## CHAP IX.IHM P1: Site web statique

[Quelques rappels](./CHAPIX.HTML_CSS/P1TPintroIHM.odt) &nbsp; [pic_bulbon.gif](./CHAPIX.HTML_CSS/img/pic_bulbon.gif) &nbsp; [pic_bulboff.gif](./CHAPIX.HTML_CSS/img/pic_bulboff.gif)

[Découverte du javascript](./CHAPIX.HTML_CSS/11_javascript/P2Javascript.odt) &nbsp; [découverte.html](./CHAPIX.HTML_CSS/11_javascript/découverte.html) &nbsp; [fonctions.js](./CHAPIX.HTML_CSS/11_javascript/fonctions.js)

## CHAP X. ARCHITECTURE DES ORDINATEURS

[Partie 1: Les portes logiques](./CHAPX.ARCHI/courporteslog.odt)

[TP portes logiques](./CHAPX.ARCHI/TPporteslogiques.odt) &nbsp; [aide_additionneur.odp](./CHAPX.ARCHI/Aide_additionneur.odp)

[Partie 2: Architecture de Von Neumann](./CHAPX.ARCHI/Cours_NSI_Architecture.odp) &nbsp; [picoprocesseur.zip](./CHAPX.ARCHI/PicoProcesseur.zip)

## CHAP XI. ALGORITHMES

[cours](./CHAPXI.ALGORITHMES/algorithme.odt)

[exercices](./CHAPXI.ALGORITHMES/exoscorrection.odt)

## CHAP XII. P-UPLETS

[TPcours ](CHAPXII.TUPLES/tuplesmramstein.odt)

## CHAP XIII. DICTIONNAIRES

[cours](./CHAPXIII.DICTIONNAIRES/cours_dicos.odp)

[TP](./CHAPXIII.DICTIONNAIRES/tp_dictionnaires.odt)

[exercices](./CHAPXIII.DICTIONNAIRES/exosdicos.odt) &nbsp; [TP_dictionnaire.py](./CHAPXIII.DICTIONNAIRES/TP_dictionnaire.py)

[exercices supplémentaires](./CHAPXIII.DICTIONNAIRES/exosdicos2.odt) &nbsp; [cygne.jpg](./CHAPXIII.DICTIONNAIRES/cygne.jpg)

## projets

[permis de conduire](./projets/projet_permis.md)

[jeu de bataille](./projets/Jeu_bataille.pdf) &nbsp; [jeu_bataille.py](./projets/jeu_bataille.py) &nbsp; [une correction](./projets/jeu_bataille_correction.py)

[projet web](./projets/Projet%20page%20Web.docx)