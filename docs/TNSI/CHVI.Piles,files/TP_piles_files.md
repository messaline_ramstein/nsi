# CHAP VI. PILES,FILES 
__Travaux pratiques__

_Avant de commencer les exercices,réaliser un module __pile.py__(sous forme de listes chaînées) et __pile_l.py__ (sous forme de listes Python) dans votre dossier afin de pouvoir appeler les constructeurs dans les exercices._

## Exercice 1
Dans le module ``pile.py``, créer la classe ``Pile`` en y incluant les méthodes ``empiler(), dépiler() et est_vide()``.
La tester en instanciant un objet de la classe ``Pile``, en y empilant quelques valeurs puis en affichant les valeurs successives de cette pile.

## Exercice 2 : QCM
1. Quelle opération ne fait pas partie de l’interface d’une pile ?

[ ] ajouter un élément à une pile

[ ] retirer l’ élément le plus récent à une pile

[ ] retirer l’élément le plus ancien à une pile

2. Quelle opération ne fait pas partie de l’interface d’une file ?

[ ] ajouter un élément à une file

[ ] retirer l’ élément le plus récent à une file

[ ] retirer l’élément le plus ancien à une file

3. Sur les quatre situations suivantes, choisir parmi les réponses suivantes la structure la plus adaptée:
[ ] une liste

[ ] un dictionnaire

[ ] une pile

[ ] une file

a. on souhaite insérer dans une structure de données les différentes hauteurs d’un arbre au fil des années sans y insérer les années.

b. dans une administration, on souhaite insérer dans uns structure de données les doléances afin de les traiter selon leur ordre d’arrivée.

c. dans une association, on souhaite enregistrer une liste d’informations pour chaque adhérent : taille, poids, etc.

d. pour un logiciel, on doit enregistrer la liste des actions dans une structure de données de sorte à voir la dernière action en priorité.

## Exercice 3 : Compléments de la classe pile 
1. Dans les modules Pile implémenté sous forme de liste Python(classe ``Pile1``) et sous forme de liste chaînée(classe ``Pile``), ajouter  deux méthodes ``sommet``( permet de connaître la valeur du sommet de la pile), ``vider`` (définir le contenu à vide).

2. Réaliser deux méthodes permettant d’inverser l’ordre dans une pile.(une pour la classe ``Pile`` et une pour la classe ``Pile1``)

## Exercice 4 : Navigateur Web 
Considérons un navigateur Web dans lequel on s’intéresse à deux opérations : aller à une nouvelle page et revenir à la page précédente. On veut que le bouton de retour en arrière permette de remonter une à une les pages précédentes, et ce jusqu’au début de la navigation.

Soient les trois URL visitées précédemment :

•  adresse_1 https://search.lilo.org/results.php?q=JO+fleuret

• adresse_2 https://www.leparisien.fr/sports/JO/jo-de-tokyo-les-fleurettistes-francaises-enargent-battues-en-finale-par-la-russie-29-07-2021-7Q7ETJSHRRGBLCSFGXDFCAX5TM.php

• adresse_3 https://www.leparisien.fr/sports/JO/direct-jo-de-tokyo-les-skaters-francais-enrevent-un-dimanche-de-medailles-25-07-2021-5CKJO2SWMFGJ5NYDB3KOJEAGRQ.php

1. Quelle structure de données va-t-on utiliser? Importer le module éventuellement dans Thonny
avec l’implémentation en liste chaînée.

2. En plus de l’adresse_courante ( la troisième URL ici) qui doit être stockée, créer la structure de données avec les adresses_précédentes( les deux premières URL).

3. Quand on navigue, l’adresse de la page courante est ajoutée à la structure de données. Ensuite l’adresse cible devient la nouvelle adresse courante. Créer la fonction ``aller_a(adresse_cible,adresse_courante)``

La tester avec l’url cible en affichant la nouvelle adresse_courante : 
adresse_4 « https://www.leparisien.fr/sports/JO/jo-de-tokyo-charline-picon-visera-lorsamedi-en-planche-a-voile-29-07-2021-7JKAUXLSTNCXDNDK7SLSVWABZE.php »

4. Pour revenir en arrière, il faut revenir sur la dernière page. Créer une fonction ``retour(adresse_courante,adresses_precedentes)`` qui vérifie  d’abord que cette page existe dans la structure de données avant de la renvoyer. Tester en affichant deux retours en arrière.

5. On souhaite avoir une fonction ``retour_avant(adresse_courante,adresses_precedentes,adresse_suivante)`` dont le comportement est :

• chaque appel à la fonction ``retour`` place la page quittée au sommet d’une seconde pile adresses_suivantes

• un appel à la fonction ``retour_avant`` récupère sommet de la pile
      ``adresses_suivantes`` comme adresse courante et met à jour les deux piles de façon adaptée,
    
• toute nouvelle navigation avec ``aller_a`` annule les adresses_suivantes.
 
Il faudra donc créer de nouvelles versions des fonctions ``aller_a()`` et ``retour()`` ayant chacune trois paramètres. Afin de tester , aller à l’adresse_3 puis réaliser un retour puis un retour avant. On doit revenir à la même adresse. 

## Exercice 5: Bon parenthésage
Pour vérifier qu'une expression est correctement parenthésée, une méthode est de parcourir le flux de caractères correspondant à l'expression, et d'utiliser une pile :

• lorsque l'on rencontre une parenthèse ouvrante on l'empile, 

• lorsque l'on rencontre une parenthèse fermante, par exemple ``]`` : 

        ◦ si la pile est vide, alors l'expression est mal parenthésée (trop de fermantes), 

        ◦ sinon, on dépile l'ouvrante située au sommet de la pile et on vérifie que les deux parenthèses correspondent (( et ), [ et ], etc.). 

Lorsque le flux de caractères a été parcouru, la pile doit être vide (pas d'ouvrante non fermée).

Par exemple, l’expression suivante est mal parenthésée :

```python
>>> is_good_par_2("( 2*{3 + 2] )") 
False
```
Créer la fonction ``is_good_par(expression)`` qui prend une chaîne de caractères en paramètre et retourne ``True`` si l’enchaînement de parenthèses est correct, ``False`` sinon. Il est conseillé d’utiliser la classe ``Pile1`` implémentée sous forme de listes Python.

## Exercice 6

![alt text](image-6.png)

1.Écrire les instructions permettant de :

• créer une file

• la remplir avec les entiers 0,2,4,6,8

• la faire afficher

• "défiler" la file en faisant afficher l’élément récupéré

2.Réaliser les méthodes ``taille(self)`` et ``sommet(self)`` qui retournent respectivement la taille de la file et le sommet de la file(le premier à sortir..) sans le supprimer.

## Exercice 7. Croisement routier

Pour simuler un croisement routier, à sens unique, on utilise 3 files ``f1``, ``f2`` et ``f3`` représentant respectivement les voitures arrivant sur des routes R1 et R2, et les voitures partant sur la route R3.La route R2 a un STOP. Les voitures de la file ``f2`` ne peuvent avancer que s’il n’y a aucune voiture sur la route R1, donc dans la file ``f1``.

![alt text](image-7.png)

On souhaite écrire un algorithme qui simule le départ des voitures sur la route R3, modélisée par la file ``f3``.

• Dans la file ``f1`` on représentera la présence d’une voiture par le nombre 1 et l’absence de voiture par 0

•  Dans la file ``f2`` on représentera la présence d’une voiture par le nombre 2 et l’absence de voiture par 0

Écrire un algorithme qui modélise ce carrefour, on utilisera une fonction ``croisement(f1,f2)`` qui prend en paramètres deux files ``f1`` et ``f2`` et qui retourne une file ``f3`` contenant la file ``f3`` des voitures sur la route R3.
 On n’utilisera que les méthodes ``enfiler, defiler, sommet et est_vide``

 On testera l’algorithme sur ``f1`` : tête <–[0, 1, 1, 0, 1]<– queue

 On testera l’algorithme sur ``f2`` : tête <–[0, 2, 2, 2, 0, 2, 0]<– queue
 
 Le résultat attendu : ``f3`` tête <–[0, 1, 1, 2, 1, 2, 2, 0, 2, 0]<– queue