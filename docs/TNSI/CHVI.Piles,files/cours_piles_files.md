# CHAP VI. PILES,FILES

## I.Pile

__1 Définition__

Une __Pile (ou Stack)__ est un type de données linéaire dans laquelle les éléments sont accessibles selon une discipline __LIFO (“Last-In First-Out”)__ : l’élément inséré en dernier est le premier à sortir.
![alt text](image-1.png)

• Insérer un élément dans la pile est appelé __empiler__ ou pousser (push)

•  Supprimer un élément de la pile est appelé __dépiler__ (pop)

L’accès aux objets de la pile se fait grâce à un unique pointeur vers le sommet de la pile, appelé __top__. 
![alt text](image-2.png)

__2 Interface__

On considère que la pile ``S`` ne contiendra que des éléments de même type. Elle a une interface proposant au moins quatre opérations :
* ``creer_pile()--> S``   :  renvoie une nouvelle pile
* ``est_vide(S)--> bool`` : renvoie ``True`` si la pile est vide, ``False`` sinon
* ``empiler(S,e)-->None``: Empile la valeur ``e`` sur la pile ``S``
* ``depiler(S)-->`` : Extrait et renvoie la valeur du sommet de la pile ``S``

__3 Implémentations__

_a) Avec une liste chaînée_

La structure de liste chaînée donne une manière élémentaire de réaliser une pile. Empiler un nouvel élément revient à ajouter une nouvelle cellule en tête de liste, tandis que dépiler un élément revient à supprimer la cellule de tête. On va ainsi construire une classe ``Pile`` définie par un unique attribut ``contenu`` associé à l’ensemble de la pile, stockés sous la forme d’une liste chaînée.

• Le constructeur défini par la méthode ``init(self)``, construit une pile vide en définissant contenu comme une liste vide.

• pour la méthode ``est_vide()``, on teste si le contenu est une liste chaînée vide.

![alt text](image-3.png)

Pour __empiler__ un nouvel élément on utilise la classe ``Cellule`` du chapitre précédent : ( on peut importer le module ``Cellule``)

```python
class Cellule:
    '''Cellule d'une liste chaînée'''
    def __init__(self,v,s):
        self.valeur=v
        self.suivante=s
```
• on crée une nouvelle Cellule qui a pour valeur l’élément e que l’on souhaite empiler

• comme cellule suivante, la première cellule de la liste d’origine (self.contenu donc)

```python
def empiler(self,e):
    '''Empile une cellule sur la pile'''
    cell=Cellule(e,self.contenu)
    self.contenu=cell
```

Pour dépiler, on doit consulter la valeur de la première cellule, si
elle existe ( sinon réaliser une levée d’exception). Il faut ensuite retirer la valeur de la liste d’origine.

```python
def depiler(self):
    '''Dépile la pile si elle n'est pas vide'''
    if self.est_vide():
        raise Indexerror('On ne peut pas dépiler une pile vide')
    else:
        e=self.contenu.valeur
        self.contenu=self.contenu.suivante
        return e
```

_b)Avec une structure de type list Python(tableau)_

On peut utiliser le type ``list`` facilement avec les méthodes connues comme ``append()`` ou ``pop()``.

```python
class Pile:

    def __init__(self):
        self.contenu=[]

    def est_vide(self):
        return len(self.contenu)==0

    def empiler(self,e):
        self.contenu.append(e)

    def depiler(self):
        if self.est_vide():
            raise Indexerror('On ne peut pas dépiler une pile vide')
        else:
            e=self.contenu.pop()
            return e
```
## II.Files

__1 Définition__

Une __File (ou Queue)__ est une structure de données linéaire dans laquelle les éléments sont accessibles selon une discipline __FIFO (“First-In First-Out”)__ : le premier élément inséré dans la liste est le premier à en sortir.

• Insérer un élément dans la file est appelé __enfiler__ (enqueue)

• Supprimer un élément de la file est appelé __défiler__ (dequeue)

![alt text](image-4.png)

L’accès aux objets de la file se fait grâce à deux pointeurs, l’un pointant sur l’élément qui a été inséré en premier et l’autre pointant sur celui inséré en dernier.

__2 L’interface__

* ``creer_file()-->F``:renvoie une nouvelle file

* ``est_vide(F)-->bool``: renvoie ``True`` si F est vide ``False`` sinon

* ``enfiler(F,e)-->None``: enfile la valeur e à la file F

* ``defiler(F)-->``: extrait la valeur en tête de F et la renvoie
      
__3 Implémentations__

_a) Avec une liste chaînée mutable_
![alt text](image-5.png)

On doit considérer la version mutable des listes chaînées. En effet , l’ajout d’un nouvel élément à l’arrière de la file : la cellule qui était la dernière avant l’ajout possède une cellule suivante.
La seconde différence est la nécessité d’accéder à la dernière valeur sans parcourir toute la liste à chaque fois. On conserve donc la dernière valeur dans un attribut de la classe ``File``.
La file vide est caractérisée par une tête et une queue vide : on peut consulter ainsi un seul des deux attributs.

```python
class File:
    '''Construire une file à l'aide d'une liste chaînéee'''
    def init__(self):
        self.tete=None
        self.queue=None

    def est_vide(self):
        return self.queue is none
```

L’ajout d’un nouvel élément à l’arrière de la file demande de créer une nouvelle cellule qui prend la dernière place et donc n’a pas de cellule suivante. Ne pas oublier le cas particulier où la liste est vide
avant.
```python
def enfiler(self,e):
    '''Enfile la valeur e en queue de file'''
    e=Cellule(e,None)
    if self.est_vide():
        self.tete=e
    else:
        self.queue.suivante=e
    self.queue=e
```

Pour retirer un élément, il faut supprimer la première cellule de la file comme dans une pile. Cependant,si la cellule est la dernière il faut redéfinir l’attribut self.queue à None.

_b) Réaliser une file avec un tableau(voir TP)_

_c)Réaliser une file avec deux piles_

Une réalisation radicalement différente de cette même structure de file consiste à utiliser deux piles,
ou directement deux listes chaînées immuables.
On peut utiliser le modèle du jeu de cartes avec une pioche (sur laquelle on prend une carte face
cachée) et une défausse ( disposée face visible ). Chacun des deux paquets de cartes est une pile, et ces deux paquets forment ensemble la réserve de
cartes :
• toute carte prise dans la réserve est retirée dans l’une de ces piles ( la pioche)
    
• toute carte remise dans la réserve est ajoutée à l’autre pile ( la défausse )
 
Une fois la pioche vide on retourne la défausse pour en faire une nouvelle pioche, laissant ainsi à la place
une défausse vide. Une file ainsi réalisée est caractérisée par deux attributs ``entree`` et ``sortie``, le premier contenant une pile dans laquelle on ajoute les nouveaux éléments et le second une pile d’où l’on prend les éléments retirés.

![alt text](image-6.png)

```python
class File:
    '''Création d'une file à l'aide de deux piles'''
    
    def __init__(self):
        self.entree=None
        self.sortie=None

    def est_vide(self):
        '''retourne true si la file est vide'''
        return self.entree is None and self.sortie is None

    def ajouter(self,e):
        '''Ajoute la valeur e sur la pile d'entree'''
        self.entree.empiler(e)

    def retirer(self):
        if not self.sortie.est_vide():
            self.sortie.depiler()
        else:
            return self.entree
```

Pour retirer un élément, l’opération est délicate :

• si la pile de sortie n’est pas vide, il suffit de dépiler son premier élément

• si la pile de sortie est vide, il faut retourner toute la pile d’entrée





