# Projet N°1     TNSI               Pour le 2 Novembre 2024

Votre fichier Python ``nom_animation.py`` sera à rendre via l’ENT à VANEHUIN MESSALINE ELISA
![alt text](image.png)

Vous allez créer votre décor animé avec le module ``tkinter`` en programmation orientée objet. Vous aurez besoin des classes :

• ``Interface`` avec, comme attributs, la fenêtre ``tk`` et le Canvas(zone graphique). Elle contiendra la méthode ``instancier(self)`` pour instancier les objets des autres classes et ``animation()`` /6

• ``Lune`` avec comme attributs ``x,y et rayo`` et la méthode ``dessiner_lune(self)`` /2

• ``Mer`` avec comme attribut ``hauteur`` et la méthode ``dessiner_mer(self)``.
Il pourrait être intéressant de récupérer la coordonnée ``y`` correspondante pour dessiner les autres objets /2

• ``Bateau`` avec comme attributs ``x,y, longueur, vitesse`` et des méthodes ``dessiner_bateau(self)``  et ``avancer(self)`` /3

• ``Etoile`` avec comme attributs ``x,y,taille`` et la méthode ``dessiner_etoile(self)``  /2

• ``Phare`` avec comme attribut son abscisse ``x`` et ses méthodes``dessiner_phare(self) et clignoter()`` ;) /3

• ``main()``   /1

Des docstrings et commentaires sont attendus !! (1)