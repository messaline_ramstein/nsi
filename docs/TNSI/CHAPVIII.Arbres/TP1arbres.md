# CHAP VIII. LES ARBRES BINAIRES
__TP1. Implémentaion d'un arbre binaire__

Les arbres binaires peuvent être implémentés de la même manière que les listes chaînées. Dans celles-ci, chaque maillon possède, au plus, un maillon suivant. Pour l’arbre, chaque nœud possède, au plus, deux nœuds suivants, un gauche et un droit. Dans ce TP, la structure de données sera implémentée à l’aide de la programmation objet.

1. En Python,on peut implémenter une classe Node représentant un nœud et possédant les 3 attributs suivants(voir cours) :

    • ``value`` : valeur de l’étiquette du nœud, de type quelconque, pas de valeur par défaut.

    • ``left ``: nœud suivant gauche, de type Node, None par défaut.

    • ``right ``: nœud suivant droit, de type Node, None par défaut.

Soit l’arbre binaire instancié par le code ci-dessous.
```python
arbre = Node('manchot')
arbre.left = Node('poule')
arbre.left.left = Node('chat')
arbre.left.left.left = Node('rat')
arbre.left.left.right = Node('chien')
arbre.left.right = Node('pigeon')
arbre.right = Node('canari')
arbre.right.left = Node('léopard')
arbre.right.right = Node('vache')
arbre.right.right.right = Node('gazelle')
```

Le représenter sur papier. Identifier sa racine et ses feuilles. 


2.Soit l’arbre binaire instancié par le code ci-dessous.
```python
noeud_A = Node("A", None, None)
noeud_E = Node("E", None, None)
noeud_I = Node("I", None, None)
noeud_K = Node("K", None, None)
noeud_L = Node("L", None, None)
noeud_J = Node("J", None, noeud_K)
noeud_B = Node("B", noeud_A, None)
noeud_D = Node("D", noeud_J, noeud_E)
noeud_H = Node("H", noeud_L, noeud_I)
noeud_C = Node("C", noeud_B, noeud_D)
noeud_G = Node("G", None, noeud_H)
noeud_F = Node("F", noeud_C, noeud_G)
arbre = noeud_F
```


 Le représenter sur papier. Identifier sa racine et ses feuilles. 

 3.Soit l’arbre binaire instancié par le code ci-dessous.

```python
arbre = Node(7, Node(11, None, Node(2, None, None)), Node(1, Node(9, None, None), Node(13, None, None)))
```

Le représenter sur papier. Identifier sa racine et ses feuilles. 

Le module Python ``binarytree`` implémente les arbres binaires exactement de la même manière que ci-dessus. De plus il possède de nombreuses fonctions pour les étudier et les afficher.

4. Importer la classe ``Node`` du module ``binarytree``

5. Importer et utiliser la fonction ``print`` pour afficher l’ arbre précédent après l’ avoir codé et vérifier ainsi votre version papier.

6. Quel est le résultat classique de la fonction ``print`` sur un objet ? Pourquoi le résultat est-il différent avec un objet du type ``Node`` du module ``binarytree`` ? On pourra examiner le code du module en cliquant sur son nom.

7. Implémenter l’arbre ci-dessous avec les trois méthodes présentées précédemment. Vérifier.
![alt text](image.png)

8. Tester si l’arbre de la question 7 est un arbre équilibré grâce à son attribut ``is_balanced``. Construire en Python un arbre permettant de vérifier le cas contraire et  tester votre exemple.

9. Tester si l’arbre de la question 7 est un arbre (presque) complet grâce à son attribut ``is_complete`` . Construire en Python un arbre permettant de vérifier le cas contraire et  tester votre exemple.

10. Tester si l’arbre de la question 7 est un arbre parfait grâce à son attribut ``is_perfect`` . Construire en Python un arbre permettant de vérifier le cas contraire et  tester votre exemple.

11. Tester si l’arbre de la question 7 est un arbre strict grâce à son attribut ``is_strict`` . Construire en Python un arbre permettant de vérifier le cas contraire et tester votre exemple.
