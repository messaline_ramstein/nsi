# CHAP VIII.ARBRES BINAIRES

__TP3. Arbres binaires de recherche__

    Définition :

    Soit un arbre binaire dont toutes les étiquettes peuvent être comparées entre elles.

    Un arbre binaire est un arbre binaire de recherche si et seulement si, pour tout noeud d'étiquette e :

    • les étiquettes de tous les noeuds de son sous-arbre gauche sont inférieures ou égales à e
    • les étiquettes de tous les noeuds de son sous-arbre droit sont supérieures à e
    • les sous-arbres gauche et droit sont eux mêmes des arbres binaires de recherche

La définition avec inférieures et supérieures ou égales est également valable et sera choisie selon les cas de figure. On peut aussi imposer l’absence de doublon.

Les arbres binaires de recherche sont donc une structure de données hiérarchique triée. Ils facilitent beaucoup d’opérations telles que la recherche, l’insertion ou la suppression de nœuds.


## I. Construction des ABR

1. Les arbres binaires ci-dessous sont-ils des ABR ? Justifier.
![alt text](image-3.png)
![alt text](image-4.png)
![alt text](image-5.png)

2. Sous Python, en utilisant la classe ``Node`` du module ``binarytree``, construire un ABR de hauteur 3, différent des précédents et le vérifier avec l’attribut ``is_bst``. Attention ! Le module définit les ABR sans doublon.

3. Compléter l’arbre binaire ci-dessous pour qu’il soit un ABR.
![alt text](image-6.png)

4. Dans quels noeuds d'un ABR se trouvent l’étiquette la plus petite et l’étiquette la plus grande ?

5. Lequel des parcours classiques des arbres binaires permet de visiter les noeuds d'un ABR dans l'ordre croissant de leurs étiquettes ? Justifier et le tester  avec Python.

6. Comment visiter ses nœuds dans l’ordre décroissant de leurs étiquettes ? Le justifier et le tester.

7. Imaginer un algorithme permettant de vérifier si un arbre binaire est un ABR.

## II. Algorithme de recherche

Rechercher dans un ABR consiste à comparer,à chaque étape du parcours, l’étiquette recherchée au nœud visité. Trois cas de figure se présentent alors :

• Si le nœud est vide : l’étiquette n’a pas été trouvée dans cette branche. On retourne ``False``.
    
• Sinon si l’étiquette cherchée est inférieure à l’étiquette du nœud : on relance la recherche sur le sous-arbre gauche et on retourne le résultat.
    
• Sinon si l’étiquette cherchée est supérieure à l’étiquette du nœud : on relance la recherche sur le sous-arbre droit et on retourne le résultat.
    
• Sinon l’étiquette du nœud est donc égale à l’étiquette cherchée. On retourne ``True`` 
.
1. A l’aide de la fonction ``build`` du module ``binarytree`` générer l’ABR contenu dans la liste ci-dessous et l’afficher. Recopier l’arbre.

```python
liste_valeurs = [7, 2, 28, 0, 4, 9, 29, None, 1, 3, 5, 8, 12, None, 30, None, None, None, None, None, None, None, 6, None, None, 10, 13]
```

2. A quel type de parcours de l’ABR correspond la liste ``liste_valeurs ``?


3. Implémenter en Python l’algorithme de recherche dans une fonction ``recherche_ABR(arbre, etiquette)`` et le tester sur cet ABR.

4. Quels sont le pire et le meilleur des cas(de squelette) pour l’algorithme de recherche dans un ABR ?



5. Comparer la complexité de l’algorithme et la hauteur de l’arbre. Justifier.



6. En déduire un encadrement de la complexité de l’algorithme de recherche dans un ABR.



## III. Algorithmes de recherche des extrema

L’étiquette la plus petite se trouve dans le nœud le plus à gauche de l’arbre. C’est le premier nœud ne possédant pas de fils gauche. L’algorithme de recherche du plus petit élément est donc le suivant.

    • Si le sous-arbre gauche du nœud est vide : on retourne la valeur du nœud
    • Sinon : on relance la recherche du minimum dans le sous-arbre gauche et on retourne le résultat de la recherche

Dans le cas de la recherche du plus grand élément on travaille avec le sous-arbre droit.

 Implémenter en Python les algorithmes de recherche du minimum et du maximum dans deux fonctions ``min_ABR(arbre)`` et ``max_ABR(arbre)`` et les tester sur l’ABR précédent.

 ## IV. Algorithme d’insertion

Insérer un nouveau nœud dans un ABR consiste à remplacer le nœud enfant vide du noeud auquel doit être attaché ce nouveau nœud. A chaque étape, on cherche donc  dans quel sous-arbre doit être inséré le nouveau nœud, le cas de base créant le nouveau nœud. La fonction doit retourner un nouvel arbre construit à partir de l’ancien.

    • Si le nœud est vide : retourner la nouvelle feuille avec la nouvelle étiquette.

    • Sinon, si l’étiquette à insérer est inférieure ou égale à l’étiquette du nœud : on retourne un nouveau sous-arbre construit avec l’étiquette du nœud, le sous-arbre gauche résultant de l’insertion et le sous-arbre droit.

    • Sinon, l’étiquette à insérer est supérieure à l’étiquette du nœud : on retourne un nouvel arbre construit avec l’étiquette du nœud, le sous-arbre gauche et le sous-arbre droit résultant de l’insertion 

La complexité de l’algorithme est la même que dans le cas de la recherche.

Implémenter en Python l’algorithme d’insertion dans une fonction ``inserer_ABR(arbre, etiquette)`` et le tester sur l’ABR précédent.

## V. Algorithme de suppression

Cet algorithme n’est pas exigible en classe de Terminale.

Puisqu’il doit d’abord trouver le nœud d’étiquette donnée avant de le supprimer, l’algorithme de suppression a la même structure que l’algorithme de recherche. Mais comme il va agir sur le squelette de l’arbre, il doit retourner, et donc construire, un nouvel arbre à chacune de ses étapes comme dans l’algorithme d’insertion.

1. Implémenter cette première partie de l’algorithme dans une fonction supprimer_ABR(arbre, etiquette). Lorsque le nœud est trouvé la fonction affichera 'Noeud trouvé. Reste à le supprimer(en supposant qu’on sait le faire)'.

2. Tester les différents cas de figure et vérifier que la fonction retourne bien l’arbre original puisque si le nœud est trouvé, il n’a pas encore été supprimé.

3. _deuxième partie :_

On modifie le code afin que :
    • si l’étiquette recherchée e est plus petite que l’étiquette du nœud actuel, on retourne un nouveau nœud dans lequel on a supprimé e du sous arbre gauche
    • si l’étiquette recherchée e est plus grande que l’étiquette de l’arbre actuelle, on retourne un nouveau nœud dans lequel on a supprimé e du sous arbre droit

4. _Troisième partie_

Si le nœud à supprimer est trouvé, quatre cas de figure se présentent alors.

    • Si le nœud est une feuille : on retourne un nœud vide .

    • Sinon si le nœud n’a qu’un fils gauche : on retourne son fils gauche 
    .
    • Sinon si le nœud n’a qu’un fils droit : on retourne son fils droit 

    • Sinon le nœud a deux fils : il faut remplacer son étiquette par l’étiquette la plus petite de son sous-arbre droit(en utilisant la fonction recherche_min) tout en supprimant le nœud comportant cette étiquette dans ce sous-arbre. L’arbre résultant est retourné au père.



 Implémenter cette troisième  partie de l’algorithme et le tester.


