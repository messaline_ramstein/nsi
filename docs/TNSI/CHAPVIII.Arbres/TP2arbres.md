# CHAPVIII. ARBRES BINAIRES 
__TP Parcours des arbres binaires__

Il faut, au préalable, créer ou importer la classe Node du module binarytree.

## I. Quelques mesures

1. Coder et tester les deux fonctions ci-dessous.

    • ``est_vide(arbre)`` : retourne ``True`` si l’arbre est vide ``False`` sinon

    • ``est_une_feuille(arbre) ``: retourne ``True`` si le nœud est une feuille, ``False`` sinon

![alt text](image-1.png)

Soit l’algorithme récursif ci-dessous.

```python
algo(arbre) :
	si arbre est vide :
		retourner la valeur 0
	sinon :
		retourner 1
			+ algo(sous arbre gauche de arbre)
			+ algo(sous arbre droite de arbre)
```

2. Que fait-il ? Expliquer.

3. Implémenter l’algorithme en lui donnant un nom adapté à sa fonction. Le tester sur l’arbre ci-contre.

4. En vous aidant de l’algorithme précédent, proposer un algorithme qui donne la hauteur d’un arbre binaire.

5. Implémenter l’algorithme dans une fonction nommée ``hauteur(arbre)`` et le tester.

6. Enfin, coder une fonction ``profondeur(arbre, etiquette)`` qui donne la profondeur du nœud d’étiquette spécifiée. Si le nœud n’est pas trouvé la fonction retournera ``None``.

## II. Parcours en profondeur(cf cours)

Parcourir un arbre consiste à visiter tous ses nœuds et à effectuer un traitement sur chacun(cf cours).  Dans ce TP, le traitement sera simplement d’afficher la valeur de son étiquette. Lors d’un parcours en profondeur, la visite d’un nœud parent est suivie récursivement de la visite de ses nœuds enfants ou inversement jusqu’à atteindre un noeud vide ou une feuille.

![alt text](image-2.png)

_• Parcours préfixe_ : le traitement est effectué avant la visite des nœuds enfants
```python
parcours_prefixe(arbre) :
	traitement
	parcours_prefixe(sous arbre gauche)
	parcours_prefixe(sous arbre droit)
```
_• parcours infixe_: le traitement est effectué entre la visite du nœud enfant gauche et du nœud enfant droit
```python
parcours_infixe(arbre) :
	parcours_infixe(sous arbre gauche)
	traitement
	parcours_infixe(sous arbre droit)
```
_• Parcours postfixe (ou suffixe)_: le traitement est effectué après la visite des nœuds enfants
```python
parcours_postfixe(arbre) :
	parcours_postfixe(sous arbre gauche)
	parcours_postfixe(sous arbre droit)
	traitement
```

1. Que donnent comme résultats les trois parcours appliqués à l’arbre du I. 

2. Implémenter les trois fonctions de parcours en profondeur. Vérifier ainsi vos réponses à la question précédente.

3. Proposer de nouvelles fonctions qui stockent les étiquettes des nœuds parcourus dans une liste plutôt que de les afficher

## III. Parcours en largeur(cf cours)

Pour l’implémentation de ce parcours, on a besoin d’une structure de file d'attente pour stocker les nœuds enfants de chaque niveau en cours de visite. L’algorithme est le suivant :
```python
parcours_largeur(arbre) :
	initialiser une file vide
	enfiler le nœud racine
	tant que la file n’est pas vide :
		défiler un nœud
		si le nœud n’est pas vide :
			traitement
			enfiler tous ses nœuds enfants
```

1. Implémenter le parcours en largeur et le tester sur l’arbre du I.

2. Proposer une nouvelle fonction qui stocke les étiquettes des nœuds parcourus dans une liste plutôt que de les afficher