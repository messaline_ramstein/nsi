# CHAP I. Modularité                                                                                    
__TP Révisions d’algo et modularité__

## I. Création d’un module

1. Coder en Python une fonction ``moyenne(tableau)`` qui calcule la moyenne des nombres du tableau.  Tester la fonction dans le programme principal qui aura comme nom ``parcours_sequentiel.py``.


2. Définir ensuite une fonction ``creation_tableau(n)`` qui retournera une liste de n  entiers naturels aléatoires entre 0 et 100.

3.Faire de même avec les  autres algorithmes de parcours séquentiel à connaître. Les noms des fonctions seront ``valeur_minimale(tableau), valeur_maximale(tableau) valeurs_extremales(tableau), premiere_occurence(elt,tableau) et occurrences(elt,tableau)``.

4. Coder aussi une fonction ``insere_element(tableau, position, element)`` à l’aide d’un parcours séquentiel.


5. Documenter les fonctions et leurs paramètres en entrée et en sortie.

A partir de maintenant, toutes les fonctions réalisées seront spécifiées.

6.Donner l’interface du module réalisé.

## II. Utilisation d’un module

1. Créer un programme qui importe le module ``parcours_sequentiel``. Son nom sera ``test_algos.py``.

2. Que fait la la fonction ``dir()`` sur le module ``parcours_sequentiel`` ?

3. Que donnent ``help(parcours_sequentiel.creation_tableau)`` et ``parcours_sequentiel.creation_tableau.__doc__``. Quelles sont les différences entre les deux ?

4. Dans le programme ``test_algos.py``,  utiliser le module ``parcours_sequentiel`` pour créer un tableau de 20 valeurs.

5. Utiliser tour à tour toutes les fonctions du module.


6. Un problème………

 Dans le fichier ``parcours_sequentiel``, ajouter les lignes de code ci-dessous :

```python
print("Ce fichier est top secret! Personne ne doit connaître ma moyenne")
print(moyenne([5,8,9,10,3]))
print("Sur 20...")
```

Relancer un fichier avec importation du module ``parcours_sequentiel``, et observer ce qu’il se passe lorsqu’on demande la  valeur minimale du tableau [6,3,2,9,13,5]


 Comment modifier le programme pour que ces tests ne soient pas exécutés quand ce module est importé par un autre programme ?

![alt text](image-1.png)

7. On s’intéresse maintenant aux performances de la fonction ``insere_element()``.

 Utiliser la méthode ``.perf_counter()`` du module ``time`` pour chronométrer son temps d’exécution dans le cas d’un tableau de 10000 valeurs.  


``t démarrage du chrono``


``Exécution des opérations(Appel de la fonction).``

``t fin d'exécution.``

``Affichage:``

``"Les opérations ont duré ………….. secondes."``

a) Pour cette fonction, quel est le meilleur des cas et le pire des cas ? Justifier.

b) Le vérifier en chronométrant son temps d’exécution dans les deux cas.

c) Vérifier que la complexité en temps de cette fonction est linéaire dans le pire des cas. La démarche et les résultats seront détaillés et commentés. Conserver les résultats de vos mesures dans un tableur.

## III. Module de tris

1. Dans un module ``tris.py``, coder l’algorithme du tri par sélection dans une fonction ``tri_selection(tableau)``. Utiliser le programme ``test_algos.py`` pour tester l’algorithme.

2. Faire de même avec le tri par insertion, fonction ``tri_insertion(tableau)``.

3. Donner l’interface du module ``tris``.


4. Serait-il judicieux de donner un caractère public ou privé aux fonctions pour le client ? Justifier et expliquer comment faire en Python.




5. Quelle est la complexité en temps de chacun de ces algorithmes ? Justifier


## IV. Une bibliothèque pour manipuler des images
Dans le domaine du traitement de l’image, nous allons nous intéresser à la bibiothèque ``PIL``. Pour
comprendre le fonctionnement il faut lire la documentation.
Nous disposons d’une image sous la forme d’un fichier ``fleur.jpg`` que nous voudrions transformer
en niveaux de gris, avec la même taille et l’enregistrer sous un ``fichier.png.`` 

1. Chercher dans la documentation les informations nécessaires pour ces opérations :
    • Quel module doit-on utiliser pour manipuler des images ?

    • Le format JPEG est-il accepté en lecture ?

    • Quelle fonction doit être utilisée ?

    • Quelle fonction d’enregistrement devra être utilisée?

2. Pour convertir en niveaux de gris, une fonction transforme les trois canaux de chacun des pixels rouge(R), vert(V), bleu(B) selon la formule :

$ `L = R * 299/1000 + G * 587/1000 + B * 114/1000`$

Cette formule est celle la plus utilisée dans les filtres selon la méthode CCIR 601.

 Trouver le nom de la fonction nécessaire pour cette conversion en mode $`L`$

3. Implémenter le code en python en suivant les étapes suivantes :
    • importer le module  ``Image``

    •  charger l’image ``fleur.jpg`` dans une variable

    • convertir l’image en niveaux de gris

    • enregistrer l’image modifiée en extension ``.png``

    • libérer la mémoire (on utilisera ``variable_image_1.close()``et ``variable_image_2.close()``)

