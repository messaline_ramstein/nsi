# Cours de TNSI Lycée Marguerite de flandre

[diapo de présentation de la TNSI ](./presTle.odp)

## CHAP I. Modularité

[cours](./CHAPI.Modularité/CHAPI.Cours_modularité.odp)

[algorithmes de 1ere à connaître](./CHAPI.Modularité/algorithmes_a_connaitre.md)

[TP](./CHAPI.Modularité/TP_algo_modu.md)

__fichiers pour le TP__

[fleurs.jpg](./CHAPI.Modularité/fleur.jpg)
[lena.jpg](./CHAPI.Modularité/lena.jpg)

[correction du DS1](./CHAPI.Modularité/DSAlgo_modu%20-%20Corrigé.odt)


## CHAP II. Récursivité

[cours récursivité](./CHAPII.Récursivité/cours_recursivité.md)

[TP Récursivité](./CHAPII.Récursivité/TP_recur.pdf)

[exercices](./CHAPII.Récursivité/exosupp.odt)

[Tours de Hanoï](./CHAPII.Récursivité/TPHanoï.odt)

[correction du DS2](./CHAPII.Récursivité/DSrec%20-%20Corr.odt)

## CHAP III. Programmation orientée objet

[cours POO](./CHAPIII.POO/cours_objets_mr.odp)

[TP POO](./CHAPIII.POO/TP_POO.md)

[exercices](./CHAPIII.POO/exoPOO.odt)

[correction DS3](./CHAPIII.POO/DSPOO_corr.odt)

## CHAP IV. Bases de données relationnelles

[cours BDR](./CHAPIV.BDR/coursBDRmramstein.odp)

[TP1 BDR](./CHAPIV.BDR/TP1_BDR.md)  &nbsp; [fiche sql](./CHAPIV.BDR/fiche_SQL.odt)

[exercices](./CHAPIV.BDR/fiche_exercices_bdr.odt)

[TP2 ciné](./CHAPIV.BDR/TP2_ciné.md) &nbsp; [cinema_V2.sqlite](./CHAPIV.BDR/cinema_v2.sqlite)

[exercices supplémentaires](./CHAPIV.BDR/exosrévisionBDR.odt)

[correction DS BDR POO](./CHAPIV.BDR/ds_db_POO_24%20_corr.odt)

## CHAP V.Listees chaînées

[cours listes chaînées](./CHV.Listes%20chaînées/cours_lch.odp)

[TP](./CHV.Listes%20chaînées/TP_lch.md)

## CHAP VI.Piles,files

[cours Piles,files](./CHVI.Piles,files/cours_piles_files.md)

[exercices Piles,Files](./CHVI.Piles,files/Exercices_piles_files.pdf)

[exercices tirés d'annales](./CHVI.Piles,files/exos_annales.docx)

[correction de l'épreuve écrite](./CHVI.Piles,files/DS_piles_files24_corr.odt) &nbsp; [correction de l'épreuve pratique](./CHVI.Piles,files/DSPiles_files_corr.py)

## CHAP VII. Gestion des processus

[cours](./CHVII.PROCESSUS/coursprocessus.odp)

[TP Processus corrigé](./CHVII.PROCESSUS/TP_processus%20-%20Corrigé.odt)

[TP interblocage corrigé](./CHVII.PROCESSUS/TP_fork_interblocage%20-%20Corrigé.odt)

[exercices](./CHVII.PROCESSUS/exproc24.odt)

[correction DS Processus et dictionnaires](./CHVII.PROCESSUS/DSprocessus25_corr.odt)

## CHAP VIII. Arbres binaires

[cours](./CHAPVIII.Arbres/cours_arbres.odp)

[exercices](./CHAPVIII.Arbres/exos_arbres.odt)

[TP1.Implémentation en Python](./CHAPVIII.Arbres/TP1arbres.md)

[TP2.Parcours des arbres](./CHAPVIII.Arbres/TP2arbres.md)

[TP3.Arbres binaires de recherche](./CHAPVIII.Arbres/TP3arbres.md)

[exercices tirés d'annales](./CHAPVIII.Arbres/exosannales.odt)

[correction DS_arbres](./CHAPVIII.Arbres/DS_arbres%20_25_corr.odt)

## CHAP IX. Routage de l'information

[rappels de 1ere](./CHAPIX.ROUTAGE/routage1.odp)

[protocole RIP](./CHAPIX.ROUTAGE/routage2.odp)

[protocole OSPF](./CHAPIX.ROUTAGE/routage3.odp)

[TD](./CHAPIX.ROUTAGE/TD_protocoles_reseaux.odt) &nbsp; [tables de routage vierges](./CHAPIX.ROUTAGE/Tables_routage_vierges.ods)

[exercices tirés d'annales](./CHAPIX.ROUTAGE/exos_routage.odt)

[correction DS Routage](./CHAPIX.ROUTAGE/)

## CHAP X. Diviser pour régner

[cours](./CHAPX.Diviser%20pour%20régner/cours_Diviser_regner.odt)

### Déroulement des algorithmes

* [fusion non récursive](./CHAPX.Diviser%20pour%20régner/SR_04_fusion_non_recursive.odp)

* [fusion récursive](./CHAPX.Diviser%20pour%20régner/SR_05_fusion_recursive.odp)

* [tri fusion](./CHAPX.Diviser%20pour%20régner/SR_06_tri_fusion.odp)

[TP](./CHAPX.Diviser%20pour%20régner/TP_Diviser_regner.odt) &nbsp; [manchot.png](./CHAPX.Diviser%20pour%20régner/manchot.png) &nbsp; [lapins.png](./CHAPX.Diviser%20pour%20régner/lapins.png)

## CHAP XI. LES GRAPHES

[cours partie 1](./CHAPXI.GRAPHES/graphes_P1.odp)

[TP cours partie 2](./CHAPXI.GRAPHES/graphes_P2.odt)

[TP cours partie 3](./CHAPXI.GRAPHES/graphes_P3.odt)

[TP Amérique](./CHAPXI.GRAPHES/exoAmerique.odt)

[exercice tiré d'annales](./CHAPXI.GRAPHES/annales_graphes.odt)

[énoncé ours en cage](./CHAPXI.GRAPHES/ours_en_cage_el.pdf) &nbsp; [fichier à compléter](./CHAPXI.GRAPHES/nsi_ours_en_cage_a_comp.py)

[correction DS sur les Graphes](./CHAPXI.GRAPHES/DS_graphes_25%20-%20Copie.odt)


## Projets

[animation tkinter](./projets/projet_animation.md) &nbsp; [aide tkinter](./projets/tkinter_animation.odt)

[labyrinthe](./projets/labyrinthe/laby.md) 

[implémentation du protocole RIP en Python](./projets/Implémentation_du_protocole_RIP_en_python.odt)

## <b>Ressources utiles</b>

[exercices sur les dictionnaires](./exodicos.odt)

[banque nationales des épreuves pratiques de NSI](https://cyclades.education.gouv.fr/delos/public/listPublicECE)


