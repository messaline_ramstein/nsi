# CHAPV.LISTES CHAINEES   
__Travaux pratiques__

## Exercice 1 : comprendre la structure de la liste chaînée 

1. Créer une liste chaînée ``l_c`` avec 3 valeurs différentes à l’aide de la classe ``Cellule`` du cours dans Thonny.
2. Que renvoient :

```python
 print(l_c) ? 
 print(l_c.valeur)? 
 print(l_c.suivante) ?
 ```

3. Faire un schéma de cette liste chaînée avec les noms des emplacements mémoire de chacun des éléments, et des flèches reliant les différentes cellules.



4. Comment atteindre la dernière valeur en utilisant une syntaxe du type de la question 2 et une boucle ?

## Exercice 2 : Affichage 

Ecrire une fonction ``affiche_liste(l_c)`` qui affiche, en utilisant la fonction ``print``, tous les éléments de la liste ``l_c`` . En écrire une première  avec une boucle ``while`` puis une récursive.

Tester ces fonctions sur la liste de l’exercice 1.

## Exercice 3 :  nième-élément
1. Ecrire une fonction itérative ``nieme_element(n,l_ch)`` qui retourne la valeur du n_ieme élément de la liste ``l_ch``

2.Ecrire une fonction récursive ``nieme_element_bis(n,l_ch)``

## Exercice 4 : Concaténation de listes
1. Créer une liste chaînée ``l_ch1`` contenant les valeurs 9,8,7 avec la classe ``Cellule`` du cours.

2. Créer une liste chaînée ``l_ch2`` contenant les valeurs 6,5,4,3.

3.a) Créer une fonction itérative ``concatener(l_c1,l_c2)`` qui retourne une liste chaînée ayant toutes les valeurs de ``l_c1`` et ``l_c2``. Cette fonction modifie  le dernier attribut suivante de ``l_c1``. 

b) Tester cette fonction sur les listes ``l_ch1`` et ``l_ch2`` et afficher les valeurs de la nouvelle liste créée.

4.Afficher le 7ième élément de la liste ``l_c1``. Que remarque-t-on? Pourquoi cela peut-il être gênant de procéder ainsi pour concaténer des listes?


## Exercice 5 : Liste d’entiers 
Ecrire une fonction ``listeN(n)`` qui reçoit en argument un entier n, supposé positif ou nul, et renvoie la liste chaînée des entiers 1,2, . . .. n __dans l’ordre__. Si n = 0, la liste renvoyée est vide. 

_Aide : On peut utiliser la classe ``Liste`` à laquelle il faudra ajouter une méthode ``afficher_liste()``_


## Exercice 6 : parcours de listes 
Soit la liste suivante:
```python
 liste_c = Cellule('l',Cellule('a',Cellule(' ',Cellule('n',Cellule('s',Cellule('i',Cellule('
',Cellule('c',Cellule("'",Cellule('e',Cellule('s',Cellule('t',Cellule(' ',Cellule('l',Cellule('a',Cellule(' ', Cellule('v',Cellule('i',Cellule('e',None)))))))))))))))))))
```

1. Ecrire une fonction ``occurences(x, liste_c)`` qui renvoie le nombre d’occurrences de la valeur x dans ``liste_c``. L’écrire de façon récursive puis avec une boucle ``while``.

2. Ecrire une fonction ``trouve(x, liste_c)`` qui renvoie le rang de la première occurrence de x dans ``liste_c``, et None sinon. L’écrire de façon récursive puis avec une boucle ``while``.

3. Ecrire une fonction récursive  ``renverser(liste_c)`` qui, à partir d’une liste du type 1, 2, 3 renvoie 3, 2, 1. 

_Aide : On peut utiliser la fonction concatener(l_c1,l_c2) définie précédemment._
