# CHAP IV. BDR                
 TP1.Découverte du langage SQL, création de tables

 On réalisera ce TP en utilisant DBBrowser qui est une application de SqLite3 qui permet d’avoir une interface graphique afin  de visualiser et manipuler les tables plus facilement.

1. Créer une base de données nommée ``test_tables.db`` en cliquant sur l’onglet ``« Nouvelle base de données »``. Pour l’instant, cette base est vide. 

2. Cliquer sur l’onglet ``« Exécuter le sql »``. C’est là qu’on va taper les instructions pour créer des tables en relation les unes par rapport aux autres.

3. Paramétrer le SGBD afin d’utiliser les contraintes d’intégrité référentielles.

```sql
PRAGMA foreign_keys = ON;
```

3. Créer la table ``groupe_fournisseurs`` avec les instructions ci-dessous. Les décrire.

```sql
CREATE TABLE groupes_fournisseurs (
	id_groupe INTEGER PRIMARY KEY,
	nom_groupe TEXT NOT NULL
);
```

4. Lancer l’exécution du sql en cliquant sur  ``« Structure de la base de données »``      et  vérifier que la table a bien été créée.

4. Quelle est sa clé primaire ?

5. Créer la table ``fournisseurs`` avec les instructions ci-dessous. 

```sql
CREATE TABLE fournisseurs (
    id_fournisseur INTEGER PRIMARY KEY,
    nom_fournisseur TEXT NOT NULL,
    id_groupe INTEGER,
    FOREIGN KEY (id_groupe)
    REFERENCES groupes_fournisseurs (id_groupe)
);
```

Attention !! Pour ne pas recréer la table précédente, il faut, soit effacer les instructions qui ont permis de créer cette table, soit les commenter de cette façon : ``--CREATE TABLE``

6. Quelle est la clé étrangère de la nouvelle table? A quelle clé primaire de quelle table fait-elle référence ?

7. Donner les schémas des deux tables en soulignant en trait plein les clés primaires et en traits pointillés les clés étrangères.  Sous la forme 
__table(clé primaire, clé étrangère, autres clés)__

8. Représenter sous forme de diagramme les schémas des deux tables et leur relation.

9. Pour l’instant, les deux tables sont vides. Insérer trois lignes dans la table groupe_fournisseurs avec les instructions ci-dessous. 

```sql
INSERT INTO groupes_fournisseurs (id_groupe, nom_groupe)
VALUES
   (1, 'mécanique'),
   (2, 'électronique'),
   (3, 'optique');
```

Vérifier que ces lignes ont bien été ajoutées à la bonne table en cliquant sur ``« Parcourir les données ».``

10. Les instructions ci-dessous permettent de lister le contenu de la table. Les essayer et les décrire.

```sql
SELECT * FROM groupes_fournisseurs;
```

11. De façon analogue à la question 9, insérer les lignes suivantes dans la table fournisseurs :
```sql
   (1, 'lextronic', 2),
   (2, 'gotronic', 2);
   ```
12. Vérifier son contenu.

13. Que retourne l’instruction suivante ? Expliquer.

```sql
INSERT INTO fournisseurs (id_fournisseur, nom_fournisseur, id_groupe)
VALUES (3, 'carrefour', 4);
```

14. Ecrire et exécuter les instructions permettant d’insérer le fournisseur ``carrefour`` dans le groupe de fournisseurs 4 nommé ``grandes_surfaces``. Justifier la démarche.

15. Vérifier le contenu des tables.

16. Effacer une ligne dans la table fournisseurs avec les instructions ci-dessous. 

```sql
DELETE FROM fournisseurs 
WHERE id_fournisseur = 3;
```

17. Vérifier les contenus des tables.

18. Que retourne l’instruction suivante ? Expliquer.

```sql
DELETE FROM groupes_fournisseurs 
WHERE id_groupe = 2;
```

19. Ecrire et exécuter les instructions permettant d’effacer le groupe de ``fournisseurs 2``. Justifier la démarche.

20. Supprimer la table ``fournisseurs`` avec les instructions ci-dessous. Les décrire.

```sql
DROP TABLE fournisseurs;
```
