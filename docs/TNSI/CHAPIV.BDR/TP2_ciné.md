# CHAP IV. BDR                                                       
__TP2. Manipulation de tables__

1. Télécharger et ouvrir  la base : ``cinema_v2.sqlite`` avec __DBBRowser__.

Le schéma relationnel de cette base est représenté par le diagramme ci-dessous. On en aura besoin pour faire les __requêtes SQL__ suivantes. On s’aidera du fichier ``fiche_sql.odt``
![alt text](image.png)

## 1 Modification, insertion et suppression
Pour chaque proposition suivante, écrire et exécuter la requête SQL correspondante.

R01 - Ajouter le réalisateur “Johnny BEGOOD” avec l’id 31 dans la table __Personnes__

R02 - Ajouter le genre “horreur” dans la table __Genres__ (sans vous soucier de la colonne idGenre)

R03 - Ajouter le cinéma “Mon salon” avec l’id 4, votre adresse personnelle, 1 salle et 4 fauteuils

R04 - Modifier le nom de l’acteur : Morgan Freeman (et pas Frimane)

R05 - Modifier la date de sortie du Films Seven : 1995 (et pas 1895)

R06 - Modifier le rôle de Leonardo DiCaprio dans Inception : Cobb (et pas Kobe)

R07 - Supprimer le réalisateur “Johnny BEGOOD” ajouté précédemment .

R08 – Supprimer  le cinéma “Mon salon” ajouté précédemment .

## 2 Modification, insertion et suppression sous contrainte

R09 - Supprimer l’acteur “Jason Statham” de la base de données. Ecrire la ou les requêtes SQL correspondante. Détailler la démarche. 

 Pour chaque requête SQL ci-dessous expliquer pourquoi l’exécution produira une erreur. Si cette erreur est liée à une contrainte d’intégrité, préciser laquelle.
 ![alt text](image-1.png)
 ![alt text](image-2.png)

 ## 3 Interrogation sur une table
 Pour chaque proposition suivante, écrire la requête SQL correspondante.

R10 - Lister le contenu complet de la table __Personnes__

R11 - Lister les titres de film et leurs années de sortie

R12 - Lister les noms et prénoms des personnes par ordre alphabétique des noms

R13 - Classer les titres des films du plus vieux au plus récent

R14 - Lister les prénoms des personnes (sans les doublons)

R15 - Lister les personnes (nom et prénom) dont le prénom est “Kevin“

R16 - Lister les films sortis dans les années 90 (de 1990 à 1999 inclus). On ne précisera que le titre.

R17 - Quelles sont les Personnes dont le nom commence par la lettre P ?

R18 - Lister les 5 films (titre et année) les plus récents

R19 - Citer les titres des Films qui sont soit sortis en 1999 ou en 2001

R20 - Déterminer l’année et le titre du film le plus récent

R21 - Calculer le nombre de salles moyen des cinémas

R22 - Calculer le nombre total de fauteuils disponibles pour tous les cinémas

R23 - Compter combien de films sont sortis en 1995

## 4 Interrogation sur plusieurs tables (jointures)
On souhaite lister tous les films (titre) correspondant au genre “Drame”. Cette requête concerne la jointure de 2 tables.  On tapera les instructions suivantes :

```sql
SELECT titre FROM films JOIN Genres ON  films.idGenre=Genres.idGenre  WHERE Genres.nom=’Drame’;
```


Pour chaque proposition suivante, écrire la requête SQL correspondante en vous inspirant de la précédente.

R24 - Donner le titre des films réalisés par David Fincher

R25  - Donner le titre et l’année de la comédie la plus récente

R26 - Lister les acteurs (nom et prénom) qui jouent dans le film “Usual Suspects”

R27 - Déterminer dans quel cinéma (nom et adresse) et à quelle date je peux visionner le film “Seven”

R28 - Lister les films (titre, date et genre) proposés par le “CGR Niort”

R29 - Afficher le plus grand cinéma (en nombre de fauteuils) pour aller voir le film “Les évadés”
