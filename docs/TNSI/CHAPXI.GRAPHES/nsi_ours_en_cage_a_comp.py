#!/usr/bin/env python3
# coding: utf-8
"""
Programme de mise en oeuvre des graphes, recherche en profondeur

Usage:
======
    python nsi_ours_en_cage.py 

 __authors__ = ("Pascal LUCAS", "Professeur NSI")
__contact__ = ("pascal.lucas@ac-lille.fr")
__version__ = "1.0.0"
__copyright__ = "copyleft"
__date__ = "20210326"

"""

from copy import deepcopy
import networkx as nx
#import matplotlib.pyplot as plt

#
#from liste_mots import *

LISTE_MOTS_0 = ["aime", "sure", "jure", "brie"]

LISTE_MOTS = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
                "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
                "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
                "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
                "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
                "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
                "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]

LISTE_MOTS_2 = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
                "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
                "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
                "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
                "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
                "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
                "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]
LISTE_MOTS_1 = ["ours", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
                "gris", "haie", "titi", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
                "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
                "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "toto",
                "cime", "cire", "tata", "cure", "dame", "dime", "dire", "ducs", "dues", "duos"]

def affichage_liste_adjacence(liste_adjacence):
    """
    Retourne une chaine de caractères formatée pour une sortie dans une console
    :param: (dict) dictionnaire des mots et des mots voisins
    :return: (str) chaine de caractères formatée mot5 --> mot1, mot2, mot3, etc... a
    :CU
    
#     >>> liste_adjacence = {'mot5': ['mot1', 'mot2', 'mot3'], 'mot100': ['mot55', 'mot12']}
#     >>> affichage_liste_adjacence(liste_adjacence)
#     mot5 --> mot1 mot2 mot3
#     mot100 --> mot55 mot12
    
    """
    chaine=""
    for mot in liste_adjacence:
        chaine= chaine + mot +" --> "
        for mot_voisins in liste_adjacence[mot]:
            chaine = chaine + mot_voisins + " "
        chaine = chaine + '\n'
    return chaine

def graphe(liste_adjacence):
    """
    Imprime le graphe établi à partir de la liste d'adjacence fournie
    :param: (dict of list) dictionnaire des noeuds et des arcs à partir de ces noeuds
    :return: (obj) Objet Digraph
    :CU
    
    """
    # Instanciation du graphe
    graphe = nx.DiGraph()
    for noeud, aretes in liste_adjacence.items():
        graphe.add_node(noeud)
        for arete in aretes:
            graphe.add_edge(noeud, arete)
    return graphe

def distance(mot_1, mot_2):
    '''
    Renvoie le nombre de lettres apparaissant dans le mot_1 qui ne sont pas présente dans le mot_2
    Si une lettre apparaît deux fois dans le mot_1, elle devra apparaitre 2 fois dans le mot_2
    pour qu'il y ait une différence nulle, 'aabc' et 'abcd' ont une différence de 1 lettre
    :param: (str) chaine de caractères
    :return: (int) nombre de lettres du mot_1 n'apparaissant pas dans le mot_2
    :CU: mot_1 et mot_2 sont des chaines de caractères non nulles, de même longueur
    
    >>> distance("abcd", "abcd")
    0
    >>> distance("abcd", "dcba")
    0
    >>> distance("abcd", "1abc")
    1
    >>> distance("abcd", "a12c")
    2
    >>> distance("abcd", "d123")
    3
    >>> distance("abcd", "aaaa")
    3
    >>> distance("abcd", "1234")
    4
    
    '''
    pass




    
          


def liste_mots_voisins(liste_mots, mot_source):
    '''
    Retourne une liste de tous les mots contenus dans la liste passée en paramètre
    dont une lettre différe du mot passé en paramètre 
    :param: (list of str) liste de mots de 4 lettres
    :param: (str) mot de caractères
    :return: (list of str) liste contenant tous les mots de distance =1
    :CU: la liste existe, les mots ont tous une longueur de 4 caractères
    
    >>> liste_mots_voisins(LISTE_MOTS, 'ours')
    ['duos', 'durs', 'fors', 'hors', 'mors', 'murs', 'purs', 'sure', 'tors']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'cire')
    ['brie', 'cime', 'cris', 'cure', 'dire', 'lire', 'mire', 'pire', 'prie', 'raie', 'rime', 'rire', 'scie', 'trie']
    
    >>> liste_mots_voisins(LISTE_MOTS, 'LEGT')
    []

    '''
    pass

def liste_adjacence(liste_mots):
    """
    Retourne le dictionnaire de la liste des mots liés pour chaque mot de la liste de mots
    {'aime': [], 'sure': ['jure'], 'jure': ['sure'], 'brie': []}
    :param: (list of str) liste de mots de 4 lettres
    :return: (dict of list) dictionnaire contenant pour chaque mot la liste des mots voisins
    :CU
    
    >>> LISTE_MOTS = ["aime", "sure", "jure", "brie"]
    >>> liste = liste_adjacence(LISTE_MOTS)
    >>> sorted(liste.items())
    [('aime', []), ('brie', []), ('jure', ['sure']), ('sure', ['jure'])]
    
    """
    pass
    
def dfs(list_adj, mot_source, mot_destination, chemin, dico_visite):
    '''
    :list_adj: (dict of list) dictionnaire contenant pour chaque mot la liste des mots voisins
    : mot_source , mot_destination : (str) mot de caractères
    :affiche(list) chemin parcouru entre le mot_source et mot_voisin
    '''
    pass
            




# Programme principal



if __name__=='__main__':
    import doctest
    doctest.testmod(verbose=False)
