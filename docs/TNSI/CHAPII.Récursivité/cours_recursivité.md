# CHAP II. LA RECURSIVITE

## I. Définition

 >>> Un algorithme <b>récursif </b> est un algorithme qui résout un problème en calculant des solutions d’instances plus petites du même problème. Ce  concept est très proche de la notion mathématique de récurrence. L’approche récursive est un des concepts de base en informatique. On  oppose généralement les algorithmes <b>récursifs </b> aux algorithmes <b> itératifs </b> qui s’exécutent sans appeler  l’algorithme lui-même. 

|![](image.png)| La wachkyrie 1914-1918 :
Léon Bel et Benjamin Rabier
![alt text](image-1.png)| Couple de mariés entre deux miroirs(libre de droit)

## II. Fonctions récursives

 >>> Une fonction est qualifiée de <b>récursive </b> si elle s’appelle elle-même.

 1. __Un premier « mauvais » exemple__

On définit ici une fonction récursive dont l’exécution ne s’arrête jamais….

```python
définition de fonction1():

    Appel de fonction1()
```

2. __L’exemple de la fonction puissance__

On peut, par exemple, implémenter de façon récursive la fonction ``puissance``. On rappelle que pour ``a`` reel non nul et ``n`` entier naturel non nul, on a :

* a<sup>0</sup> =1

* a<sup>n</sup>=a*a<sup>(n-1)</sup>

Notons alors  ``deux_puiss(n)`` la fonction d’argument l’entier ``n`` et qui renvoie 2<sup>n</sup>.
On peut programmer cette fonction de façon récursive :

```python 
def deux_puiss(n):
    if n==0: ##cas de base
        return 1
    else: 
        return 2 * deux_puiss(n-1) ## Appel récursif
```

Voyons comment va fonctionner cette fonction ``deux_puiss(n)``.

Eléments caractéristiques :

1. Il faut au moins une situation qui ne consiste pas en un appel récursif

```python
	if n== 0 :
		return 1
```
Cette situation est appelée __situation de terminaison__ ou __situation d’arrêt__ ou __cas d’arrêt__ ou __cas de base__.

2. Chaque appel récursif doit se faire avec des données qui permettent de se rapprocher d’une situation de terminaison.

```python
    return 2*deux_puiss(n-1)
```

Il faut s’assurer que la situation de terminaison est atteinte __après un nombre fini d’appels récursifs__.
La preuve de terminaison d’un algorithme récursif se fait en identifiant la construction d’une __suite strictement décroissante d’entiers positifs ou nuls__.

_Pile d’exécution_

Observons sur Python Tutor le déroulé : https://pythontutor.com/visualize.html#mode=edit

1. Lors de l’appel une structure de __Pile est utilisée__. Cette notion sera vue de manière plus approfondie dans un chapitre ultérieur.

2. L’idée est que, comme pour une pile d’assiettes, on peut :

    • Soit empiler un objet en haut de la pile;

    • Soit retirer un objet du haut.

3. La pile d’exécution est limitée par défaut à 1 000 sous python (voir 997 sous repl?). Au delà, on a une erreur :
```python
"RecursionError : maximum recursion depth exceeded in comparison".
```

![alt text](image-2.png)

## III. Récursivité terminale

__1. Définition__

>>>¤ Un algorithme récursif simple est <b>terminal</b> lorsque l’appel récursif est le dernier calcul effectué pour obtenir le résultat. Cette instruction est alors nécessairement « pure », c'est-à-dire qu'elle consiste en un simple appel à la fonction, et jamais à un calcul ou une composition .

![alt text](image-3.png)

Dans le premier cas, aucune référence aux résultats précédents n'est à conserver en mémoire, tandis que dans le second cas, tous les résultats intermédiaires doivent l'être. Les algorithmes récursifs exprimés à l'aide de fonctions à récursion terminale profitent donc d'une optimisation de la pile d'exécution. 

La fonction ``deux_puiss`` précédente n’est donc pas terminale…. En effet, la dernière instruction est une composition faisant intervenir l’appel récursif.

![alt text](image-4.png)

_Exemple d’algorithme récursif terminal:_

Prédicat de présence d’un caractère dans une chaîne : 
Un caractère c est présent dans une chaîne s non vide, s’il est le premier caractère de s ou s’il est présent dans les autres caractères de s. Il n’est pas présent dans la chaîne vide.

```python
def present(c,s):
'	'' exemple de récursivité terminale
	'''
	if len(s) ==0 :
		return False
	elif c==s[0] :
		return True
	else :
		return present(c,s[1:])
```

_Remarque : Lors de la compilation, la récursion terminale peut être transformée en itération, c’est à dire une série d’étapes séquentielles.(boucle for...)_

2.  Rendre terminal un algorithme récursif
On peut utiliser un accumulateur, passé en paramètre, pour calculer le résultat au fur et à mesure des
appels récursifs. La valeur de retour du cas de base devient la valeur initiale de l’accumulateur et lors
d’un appel récursif, le “calcul en attente” sert à calculer la valeur suivante de l’accumulateur. Ainsi
on obtient :

```python
def occurrences_term(c,s, acc = 0):
	'''recherche du nombre d'occurrences d'un caractère dans une chaîne
	c: type str : caractère recherché
	s: type str : chaine
	sortie acc : type int ; nombre d'occurences dans la chaine
'	''
	if s == "":
		return acc # valeur de retour du cas de base
	elif c == s[0]:
		return occurrences_term(c,s[1:], acc + 1) # caractère présent : accumulateur augmenté # 										poursuite de la récursivité
	else:
		return occurrences_term(c,s[1:], acc) # caractère absent : poursuite de la récursivité
```



Il existe d’autres définitions récursives plus riches : des cas récursifs multiples (différents appels selon la parité par exemple) , double récursion ( plusieurs appels en cours de définition) , imbriquée, mutuelle etc. . ..


