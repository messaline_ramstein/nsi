# CHAP III. PROGRAMMATION ORIENTEE OBJET

__Travaux pratiques__

## EX1. Définir et utiliser une classe

1. Dans un module ``vehicules.py``, créer la classe ``Voiture()`` vue en cours. __Ne pas oublier de tout documenter tout au long du TP !__

2. Instancier deux objets ``voiture_1`` et ``voiture_2`` à partir de cette classe, le premier avec les paramètres par défaut, le second avec des paramètres différents. Que retournent ``print(voiture_1)`` et ``print(voiture_2)`` ?

3. Utiliser la méthode ``caractéristiques()`` pour obtenir les valeurs des attributs.

4. Même si c’est à éviter, accéder directement aux valeurs des attributs.

5. Utiliser la méthode ``modifie_couleur()`` pour modifier la couleur de l’objet ``voiture_1``.

6. Même si c’est à éviter, modifier directement son attribut ``puissance``. Vérifier.

7. Ajouter les méthodes manquantes permettant de modifier correctement les valeurs des attributs.

8. Ajouter les attributs ``nombre_portes, boite_vitesse, masse_vide,masse, masse_PTAC`` (Poids Total Autorisé en Charge).

9. Modifier la méthode ``caractéristiques()`` pour qu’elle retourne les valeurs de tous les attributs.

10. Cette méthode retourne un dictionnaire. Décrire cette structure de donnée.

11. Les dictionnaires Python sont eux-mêmes des objets. Que font leurs méthodes ``.keys(), .values() et .items()``.

12. Les utiliser dans des boucles pour afficher successivement toutes les clés de ``voiture_2.caracteristiques()``, puis toutes ses valeurs et enfin toutes ses paires (clé, valeur).

13. Créer une méthode à la classe ``Voiture()`` permettant d’afficher les caractéristiques sous la forme :

```python
Les caractéristiques du véhicule sont :
    moteur : essence
    couleur : violet
… etc …
```

14. Créer une méthode ``charge()`` qui augmente la masse du véhicule. La masse sera modifiée et la méthode retournera ``True`` si la nouvelle masse est autorisée, ``False`` sinon.

15. Créer une méthode ``decharge()`` qui diminue la masse du véhicule. La masse sera modifiée et la méthode retournera ``True`` si la nouvelle masse est autorisée, ``False`` sinon.

16. Passer quelques méthodes en privé et vérifier que c’est le cas avec la fonction ``help()``. Montrer avec la fonction ``dir()`` que rien n’est vraiment privé en Python. Les rendre à nouveau publiques.

## EX2. Vecteurs

1. Dans un module  ``maths_a_moi.py``, créer une classe ``Vecteur()``. Un vecteur sera modélisé par ses trois composantes ``x, y et z``.

2. Construire et tester les méthodes suivantes :

    • ``set_composantes_xyz()``: fixe les composantes du vecteur.

    • ``set_composantes_AB()``: fixe les composantes du vecteur à partir des coordonnées des deux points A et B qui le définissent.

    • ``get_composantes()`` : retourne les coordonnées du vecteur sous la forme d’un tuple.

    • ``addition()``: effectue l’addition de deux vecteurs. Le résultat remplace les coordonnées du vecteur appelant la méthode.

    • ``soustraction()`` : effectue la soustraction de deux vecteurs.

    • ``norme()`` : retourne la norme du vecteur.

    • ``produit_scalaire()``: retourne le produit scalaire de deux vecteurs.

    • ``angle()`` : donne l’angle en radian entre deux vecteurs.

    • ``is_colineaire()`` : retourne ``True`` si le vecteur est colinéaire à un autre.

    • ``is_orthogonal()`` : retourne ``True`` si le vecteur est orthogonal à un autre

## EX3. Polynômes

1. Toujours dans le module ``maths_a_moi.py``, créer une classe ``Polynome``. Un polynôme sera modélisé par une liste dont les éléments correspondent aux coefficients de ses différents degrés.

2. Construire une première méthode ``degre()`` donnant le degré du polynôme.

3. Dans un premier temps, ajouter deux méthodes ``addition()`` et ``soustraction()`` permettant d’additionner et de soustraire un autre polynome  .

4. Dans un second temps, ajouter deux méthodes ``multiplication()`` et ``division()`` permettant de multiplier et de diviser deux polynômes.

5. Ajouter une méthode privée ``discriminant_degre2()`` calculant le discriminant du polynôme s’il est de degré 2. La méthode stoppera le programme avec le message Le degré doit être de 2 si ce n’est pas le cas. On utilisera pour cela une __assertion__ avec le mot clé ``assert``.

6. Utiliser cette méthode dans une méthode ``racines_degre2()`` calculant les racines réelles du polynôme s’il est de degré 2. La méthode stoppera le programme avec le message Le degré doit être de 2 si ce n’est pas le cas. La méthode retournera ``None`` s’il ce n’a pas de racines réelles.

## EX4. La  classe ‘Temps’

En Python, écrire une classe Temps qui permet de définir un horaire au format __hh:mm:ss__ ( 3 attributs heures,minutes, et secondes) et qui admet les méthodes suivantes :

 •  ``affiche`` qui affiche l’horaire au format ” 12 h 37 min 47 s ” ;

 • ``add`` qui ajoute deux horaires de la classe ``Temps`` ;

 • ``sub`` qui calcule la différence entre deux horaires de la classe ``Temps``.



## EX5 : Tableaux indexés au choix 

Dans certains langages de programmation, les tableaux ne sont pas nécessairement indexés à partir de 0.
On se propose dans cet exercice de construire une classe ``Tableau`` pour réaliser de tels tableaux. Un objet de cette classe aura deux attributs, un attribut ``premier`` qui est la valeur du premier indice et un attribut ``contenu`` qui est un tableau Python contenant les éléments. Ce dernier est un tableau indexé à partir de 0.

1. Ecrire le constructeur ``init(self, imin, imax, v)`` où ``imin`` est le premier indice, ``imax`` est le dernier indice, et ``v`` la valeur utilisée pour initier toutes les cases du tableau.

``t = Tableau(-10,9,42)`` est un tableau de 20 cases indexées de -10 à 9 toutes initialisées avec la valeur 42.

2. Ecrire une méthode ``longueur(self)`` qui renvoie la taille du tableau.

3. On va utiliser l’indice i dans le tableau : écrire  une méthode  ``indice_valide(self , i)`` permettant de vérifier que l’indice i est bien valide et, dans le cas contraire, lever l’exception ``IndexError`` avec la valeur i en argument ( ``raise IndexError(i)``).  

4. Ecrire une méthode  ``getitem(self , i)`` qui renvoie l’élément du tableau d’indice i. De même, écrire une méthode ``setitem(self , i, v)`` qui modifie l’élément du tableau ``self`` d’indice i pour lui donner la valeur ``v``. On exploitera la méthode validant la valeur de l’indice i précédente.

5. Ecrire une méthode ``affichage(self)`` qui renvoie une chaîne de caractères décrivant le contenu
du tableau.