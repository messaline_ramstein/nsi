from tkinter import*
from random import*
from time import sleep
###création de la fenetre
fenetre=Tk()
fenetre.title('Snake')
zone_graphique=Canvas(fenetre,height=600,width=600,bg='black')
zone_graphique.pack()
height=600
width=600
snake=[(300,300),(300,310),(300,320)]

x_tete,y_tete=(30,30)
mouvement=(0,height//60)
keypressed=None

####Affichages
def dessiner_carre(x,y,couleur):
    zone_graphique.create_rectangle(x,y,x+10,y+10,fill=couleur)
    
def dessiner_liste_carres(liste_carres,couleur):
    for elt in liste_carres:
        dessiner_carre(elt[0],elt[1],couleur)
def afficher_score(score):
    zone_graphique.create_text(30,20,text=f"score:{score}",fill='white')
    

# dessiner_liste_carres([(50,50),(50,60),(50,70)],'red')
# afficher_score(score)

###Position des pommes
def position_aleatoire(height,width):
    x=randint(0,59)
    y=randint(0,59)
    return x*width//60,y*height//60

def position_valide():
    global x_tete
    global y_tete
    global width
    global height
    return 0<=x_tete<=width and 0<=y_tete<=height

###déplacement en fonction de la touche appuyée
def gestion_evenements_clavier(evenement):
    global keypressed
    touche = evenement.keycode
    if touche == 39:
        keypressed = 'right'
    elif touche == 37:
        keypressed = 'left'
    elif touche == 40:
        keypressed ='down'
    elif touche == 38:
        keypressed ='up'

def changer_mouvement():
    global keypressed
    global mouvement
    if keypressed=='left':
        mouvement=(-width//60,0)
    elif keypressed=='right':
        mouvement=(width//60,0)
    elif keypressed=='down':
        mouvement=(0,height//60)
    elif keypressed=='up':
        mouvement=(0,-height//60)
    return mouvement
        
        
    
    

def position_tete(liste_positions):
    return liste_positions[-1]

def bouge(liste_positions,pomme):
    global mouvement
    global x_tete
    global y_tete
    global height
    global width
    x_tete,y_tete=position_tete(liste_positions)
    
    if mouvement==(-width//60,0):
        x_tete-=width//60
    elif mouvement==(width//60,0):
        x_tete+=height//60
    elif mouvement==(0,height//60):
        y_tete+=height//60
    elif mouvement==(0,-height//60):
        y_tete-=height//60
    liste_positions.append((x_tete,y_tete))
    liste_positions.pop(0)
    
    
    
         
zone_graphique.focus_set()
zone_graphique.bind('<Key>',gestion_evenements_clavier)


def jeu():
   
    global width
    global height
    global mouvement
    global x_tete
    global y_tete
    global snake
    
    score=0
    pomme=position_aleatoire(width,height)
    while position_valide():
        zone_graphique.delete('all')
        afficher_score(score)
        
        dessiner_carre(pomme[0],pomme[1],'red')
        bouge(snake,pomme)
        if x_tete==pomme[0] and y_tete==pomme[1]:
            snake.append((x_tete+mouvement[0],y_tete+mouvement[1]))
            pomme=position_aleatoire(width,height)
            dessiner_carre(pomme[0],pomme[1],'red')
            
        dessiner_liste_carres(snake,'green')
        score=len(snake)
        changer_mouvement()
        
        zone_graphique.update()
        sleep(0.2)
    zone_graphique.create_text(300,300,text='GAME OVER',fill='white')

jeu()

fenetre.mainloop()